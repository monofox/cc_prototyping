unit UCustomerEdit;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UCustomer,
  UInput,
  UInputButton,
  UInputNumber,
  UInputText,
  UPaging;

type

TCustomerEditor = class
  private
    FCustomer:    TCustomer;
    
    FNumber:      TInputNumber;
    FName:        TInputText;
    FFirstName:   TInputText;
    FPhone:       TInputText;
    
    FBtnCancel:   TInputButton;
    FBtnCont:     TInputButton;
    FBtnSave:     TInputButton;
    FMultiple:    Boolean;
  
  public
    constructor Create(Customer: TCustomer; Multiple: Boolean);
    procedure   Draw;
    function    Save: Boolean;
    function    Show: TInputAction;
end;

procedure CustomerCreate;
procedure CustomerEdit(Paging: TPaging);

implementation

uses
  UData,
  USort,
  UStringUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF};

var
  Keys: array[0..1] of TKey = 
  (
    ('Tab', 'Zum nächsten Feld'),
    ('Esc', 'Zurück')
  );
  ErrorMsg: array of String;

constructor TCustomerEditor.Create(Customer: TCustomer; Multiple: Boolean);
begin
  Self.FCustomer := Customer;
  
  Self.FMultiple := Multiple;
  
  Self.FBtnCancel := TInputButton.Create('Abbrechen');
  Self.FBtnSave   := TInputButton.Create('Speichern');
  
  if Customer = nil then
  begin
    Self.FBtnCont := TInputButton.Create('Speichern und weiteren Kunden anlegen');
    
    Self.FNumber    := TInputNumber.Create(1, 1, 6000, true);
    Self.FName      := TInputText.Create('', 100, 10);
    Self.FFirstName := TInputText.Create('', 100, 10);
    Self.FPhone     := TInputText.Create('', 100, 10);
  end
  else
  begin
    Self.FBtnCont := TInputButton.Create('Speichern und nächsten Kunden bearbeiten');
    
    Self.FNumber    := TInputNumber.Create(Customer.Number, 1, 6000, true);
    Self.FName      := TInputText.Create(Customer.Name, 100, 10);
    Self.FFirstName := TInputText.Create(Customer.FirstName, 100, 10);
    Self.FPhone     := TInputText.Create(Customer.Phone, 100, 10);
  end;
end;

procedure TCustomerEditor.Draw;
var 
  I: Integer;
begin
  PrintHead('Kunden bearbeiten');
  PrintHelp(Keys);
  PrintLn;
  
  Print('Nummer:   ');
  Self.FNumber.SetPosition;
  Self.FNumber.Draw;
  PrintLn;
  
  Print('Nachname: ');
  Self.FName.SetPosition;
  Self.FName.Draw;
  PrintLn;
  
  Print('Vorname:  ');
  Self.FFirstName.SetPosition;
  Self.FFirstName.Draw;
  PrintLn;
  
  Print('Telefon:  ');
  Self.FPhone.SetPosition;
  Self.FPhone.Draw;
  PrintLn;
  
  if Self.FMultiple then
  begin
    PrintLn;
    Self.FBtnCont.SetPosition;
    Self.FBtnCont.Draw;
  end;
  
  PrintLn;
  Self.FBtnSave.SetPosition;
  Print('               ');
  Self.FBtnCancel.SetPosition;
  
  Self.FBtnSave.Draw;
  Self.FBtnCancel.Draw;

  // Draw error messages
  if (Length(ErrorMsg) > 0) then
  begin
    PrintLn;
    PrintLn;
    TextColor(Red);
    PrintLn('Beim Speichern sind folgende Fehler aufgetreten:');
    for I := 0 to High(ErrorMsg) do
    begin
      PrintLn('> ' + ErrorMsg[I]);
    end;
    NormVideo;
  end;
end;

function TCustomerEditor.Show: TInputAction;
begin
  Result := SDiscard;
  // At the beginning, we have no errors.
  SetLength(ErrorMsg, 0);

  while true do
  begin
    Self.Draw;
    
    if Self.FNumber.Activate = SEscape then
      break;
    
    if Self.FName.Activate = SEscape then
      break;
    
    if Self.FFirstName.Activate = SEscape then
      break;

    if Self.FPhone.Activate = SEscape then
      break;
    
    if Self.FMultiple then
    begin
      case Self.FBtnCont.Activate of
        SSpace, SReturn:
        begin
          if Self.Save then
          begin
            Result := SCont;
            break
          end;
          Self.Draw;
        end;
        
        SEscape:
          break;
      end;
    end;
    
    case Self.FBtnSave.Activate of
      SSpace, SReturn:
      begin
        if Self.Save then
        begin
          Result := SSave;
          break;
        end;
        Self.Draw;
      end;
      
      SEscape:
        break;
    end;
    
    if Self.FBtnCancel.Activate <> STab then
      break;
  end;
end;

function TCustomerEditor.Save: Boolean;
var
  Customer: TCustomer;
begin
  Result := true;
  SetLength(ErrorMsg, 0);

  // Set message
  if Length(Self.FName.Value) <= 0 then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Die Eingabe des Namen ist Pflicht!';
    Result := false;
  end;

  if Length(Self.FFirstName.Value) <= 0 then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Die Eingabe des Vornamens ist Pflicht!';
    Result := false;
  end;

  if Trunc(Self.FNumber.Value) <= 0 then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Die Eingabe der "Bezeichnung" ist Pflicht!';
    Result := false;
  end
  else
  begin
    // Check whether the number does already exist!
    // Only check, if its not changed
    if ((Self.FCustomer <> nil) and (Self.FCustomer.Number <> Self.FNumber.Value)) or
       (Self.FCustomer = nil) then
    begin
      if Data.GetCustomerByNumber(Trunc(Self.FNumber.Value)) > -1 then
      begin
        SetLength(ErrorMsg, Length(ErrorMsg) + 1);
        ErrorMsg[High(ErrorMsg)] := 'Es existiert bereits ein Kunde mit der gegebenen Nummer!';
        Result := false; 
      end;
    end;
  end;


  if (Self.FCustomer = nil) and Result then
  begin
    Customer := TCustomer.Create(Trunc(Self.FNumber.Value),
                                 Self.FName.Value,
                                 Self.FFirstName.Value,
                                 Self.FPhone.Value);
    
    Data.AddCustomer(Customer);
  end
  else if Result then
  begin
    if Self.FCustomer.Number <> Trunc(Self.FNumber.Value) then
      Data.UpdateReferenceCustomer(Self.FCustomer.Number, Trunc(Self.FNumber.Value));

    Self.FCustomer.Number    := Trunc(Self.FNumber.Value);
    Self.FCustomer.Name      := Self.FName.Value;
    Self.FCustomer.FirstName := Self.FFirstName.Value;
    Self.FCustomer.Phone     := Self.FPhone.Value;
  end;
end;

procedure CustomerCreate;
var
  Editor: TCustomerEditor;
begin
  while true do
  begin
    Editor := TCustomerEditor.Create(nil, true);
    if Editor.Show <> SCont then
      break;
  end;
  SortCustomers;
end;

procedure CustomerEdit(Paging: TPaging);
var
  Editor: TCustomerEditor;
  Count, I: Integer;
begin
  Count := Paging.CountSelected;
  if Count = 0 then
  begin
    Editor := TCustomerEditor.Create(Data.Customers[Paging.SelectedIndex], false);
    Editor.Show;
  end
  else
  begin
    for I := 0 to Paging.Length - 1 do
    begin
      if not Paging.IsSelected(I) then
        continue;
      
      Editor := TCustomerEditor.Create(Data.Customers[I], true);
      Editor.Show;
    end;
  end;
  SortCustomers;
end;

end.

