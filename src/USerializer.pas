unit USerializer;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UCamper,
  UCamperClass,
  UCustomer,
  UCustomerType,
  UReservation;

procedure LoadCampers      (Data: AnsiString; var Campers:       TCampers);
procedure LoadCamperClasses(Data: AnsiString; var CamperClasses: TCamperClasses);
procedure LoadCustomers    (Data: AnsiString; var Customers:     TCustomers);
procedure LoadCustomerTypes(FileName: String; var CustomerTypes: TCustomerTypes);
procedure LoadReservations (Data: AnsiString; var Reservations:  TReservations);
function SaveCampers      (Campers:       TCampers):       AnsiString;
function SaveCamperClasses(CamperClasses: TCamperClasses): AnsiString;
function SaveCustomers    (Customers:     TCustomers):     AnsiString;
function SaveReservations (Reservations:  TReservations):  AnsiString;

implementation

uses
  Classes,
  SysUtils;

procedure LoadCampers(Data: AnsiString; var Campers: TCampers);
var
  List: TStringList;
  I: Integer;
begin;
    List := TStringList.Create;
    List.DelimitedText := Data;
    
    SetLength(Campers, List.Count);
    for I := 0 to List.Count - 1 do
      Campers[I] := TCamper.Deserialize(List[I]);
    FreeAndNil(List);
end;

procedure LoadCamperClasses(Data: AnsiString; var CamperClasses: TCamperClasses);
var
  List: TStringList;
  I: Integer;
begin;
    List := TStringList.Create;
    List.DelimitedText := Data;
    
    SetLength(CamperClasses, List.Count);
    for I := 0 to List.Count - 1 do
      CamperClasses[I] := TCamperClass.Deserialize(List[I]);
    FreeAndNil(List);
end;

procedure LoadCustomers(Data: AnsiString; var Customers: TCustomers);
var
  List: TStringList;
  I: Integer;
begin;
    List := TStringList.Create;
    List.DelimitedText := Data;
    
    SetLength(Customers, List.Count);
    for I := 0 to List.Count - 1 do
      Customers[I] := TCustomer.Deserialize(List[I]);
    FreeAndNil(List);
end;

procedure LoadCustomerTypes(FileName: String; var CustomerTypes: TCustomerTypes);
var
  List: TStringList;
  I: Integer;
begin;
    List := TStringList.Create;
    List.LoadFromFile(FileName);
    
    SetLength(CustomerTypes, List.Count);
    for I := 0 to List.Count - 1 do
      CustomerTypes[I] := TCustomerType.Deserialize(List[I]);
    FreeAndNil(List);
end;

procedure LoadReservations(Data: AnsiString; var Reservations: TReservations);
var
  List: TStringList;
  I: Integer;
begin;
    List := TStringList.Create;
    List.DelimitedText := Data;
    
    SetLength(Reservations, List.Count);
    for I := 0 to List.Count - 1 do
      Reservations[I] := TReservation.Deserialize(List[I]);
    FreeAndNil(List);
end;

function SaveCampers(Campers: TCampers): AnsiString;
var
  List: TStringList;
  I: Integer;
begin;
  List := TStringList.Create;
  
  for I := 0 to Length(Campers) - 1 do
    List.Add(Campers[I].Serialize);
  
  Result := List.DelimitedText;
  FreeAndNil(List);
end;

function SaveCamperClasses(CamperClasses: TCamperClasses): AnsiString;
var
  List: TStringList;
  I: Integer;
begin;
  List := TStringList.Create;
  
  for I := 0 to Length(CamperClasses) - 1 do
    List.Add(CamperClasses[I].Serialize);
  
  Result := List.DelimitedText;
  FreeAndNil(List);
end;

function SaveCustomers(Customers: TCustomers): AnsiString;
var
  List: TStringList;
  I: Integer;
begin;
  List := TStringList.Create;
  
  for I := 0 to Length(Customers) - 1 do
    List.Add(Customers[I].Serialize);
  
  Result := List.DelimitedText;
  FreeAndNil(List);
end;

function SaveReservations(Reservations: TReservations): AnsiString;
var
  List: TStringList;
  I: Integer;
begin;
  List := TStringList.Create;
  
  for I := 0 to Length(Reservations) - 1 do
    List.Add(Reservations[I].Serialize);
  
  Result := List.DelimitedText;
  FreeAndNil(List);
end;

end.
