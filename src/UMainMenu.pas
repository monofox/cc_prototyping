unit UMainMenu;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UPaging;

type

TMainMenuItem = (SEditReservations, SEditCustomers, SEditCampers, SEditCamperClasses, SExit);

TMainMenu = class
  private
    Exit:     Boolean;
    Paging:   TPaging;
    StartPos: Integer;
    
    procedure DrawInterface;
    procedure Enter;overload;
    procedure Enter(Item: TMainMenuItem);overload;
    procedure PrintItem(Text: String);
  
  public
    constructor Create;
    procedure Show;
end;

procedure ShowMainMenu;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UCamperClassList,
  UCamperList,
  UCustomerList,
  UReservationMenu,
  UStringUtils;

var
  Keys: array[0..2] of TKey =
  (
    ('Esc', 'Beenden'),
    ('Pfeiltasten', 'Auswählen'),
    ('Enter', 'Öffnen')
  );

constructor TMainMenu.Create;
begin
  Self.Exit := false;
  Self.Paging := TPaging.Create(Integer(High(TMainMenuItem)) + 1, 18, false);
end;

procedure TMainMenu.DrawInterface;
var
  I: TMainMenuItem;
begin
  PrintHead('Hauptmenü');
  PrintHelp(Keys);
  PrintLn;
  
  Self.StartPos := WhereY;
  
  for I := Low(TMainMenuItem) to High(TMainMenuItem) do
  begin
    case I of
      SEditReservations:
        Self.PrintItem('_Buchungen anzeigen/bearbeiten');
      
      SEditCustomers:
        Self.PrintItem('_Kunden anzeigen/bearbeiten');
      
      SEditCampers:
        Self.Printitem('_Camper anzeigen/bearbeiten');
      
      SEditCamperClasses:
        Self.PrintItem('Camper-_Typen anzeigen/bearbeiten');
      
      SExit:
        Self.PrintItem('B_eenden');
    end;
  end;
  
  Cursor(1, Self.StartPos+Self.Paging.SelectedIndex-Self.Paging.StartIndex);
end;

procedure TMainMenu.Enter;
begin
  case TMainMenuItem(Self.Paging.SelectedIndex) of
    SEditReservations:
      ShowReservationMenu;
    
    SEditCustomers:
      ShowCustomerList;
    
    SEditCampers:
      ShowCamperList;
    
    SEditCamperClasses:
      ShowCamperClassList;
    
    SExit:
      Self.Exit := true
  end;
end;

procedure TMainMenu.Enter(Item: TMainMenuItem);
begin
  Self.Paging.SelectedIndex := Integer(Item);
  Self.Enter;
end;

procedure TMainMenu.PrintItem(Text: String);
var
  Position: Integer;
begin
  Position := Pos('_', Text);
  if Position = 0 then
    PrintLn('* ' + Text)
  else
  begin
    Print('* ' + Copy(Text, 1, Position-1));
    HighVideo;
    Print(Text[Position+1]);
    NormVideo;
    PrintLn(Copy(Text, Position+2, Length(Text)));
  end;
end;

procedure TMainMenu.Show;
var
  KeyCode, ScanCode: Char;
begin
  while not Self.Exit do
  begin
    Self.DrawInterface;
    
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        ScanCode := ReadKey;
        case ScanCode of
          #72: // Pfeil hoch
            Self.Paging.SelectPrev;
          
          #80: // Pfeil runter
            Self.Paging.SelectNext;
        end;
      end;
      
      #13, ' ': // Enter, Leertaste
        Self.Enter;
      
      #27, // Esc
      'e':
        Self.Enter(SExit);
      
      'b':
        Self.Enter(SEditReservations);
      
      'k':
        Self.Enter(SEditCustomers);
      
      'c':
        Self.Enter(SEditCampers);
      
      't':
        Self.Enter(SEditCamperClasses);
    end;
  end;
end;


procedure ShowMainMenu;
var
  Menu: TMainMenu;
begin
  Menu := TMainMenu.Create;
  Menu.Show;
  ClrScr;
end;

end.

