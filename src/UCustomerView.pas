unit UCustomerView;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UPaging;

procedure CustomerView(Paging: TPaging);
procedure CustomerViewInterface(CustomerNr: Integer);

implementation

uses
  SysUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UCustomer,
  UCustomerType,
  UData,
  UStringUtils;

var
  Keys: array[0..0] of TKey = (('Beliebige Taste', 'Zurück'));

procedure CustomerView(Paging: TPaging);
var
  Count, I: Integer;
begin
  
  Count := Paging.CountSelected;
  if Count = 0 then
  begin
    CustomerViewInterface(Paging.SelectedIndex);
  end
  else
  begin
    for I := Paging.Length - 1 downto 0 do
    begin
      if not Paging.IsSelected(I) then
        continue;
         
      CustomerViewInterface(I);
      Paging.Unselect(I);
    end;
  end;
  
  Paging.Length := Length(Data.Customers);
end;

procedure CustomerViewInterface(CustomerNr: Integer); 
var
  Number:       Integer;
  Name:         String;
  FirstName:    String;
  Phone:        String;
  Customer:     TCustomer;
  CustomerType: TCustomerType;

begin
  // We have to load the existent data for the customer.
  Customer  := Data.Customers[CustomerNr];
  Number    := Customer.Number;
  Name      := Customer.Name;
  FirstName := Customer.FirstName;
  Phone     := Customer.Phone;
  CustomerType := Data.GetCustomerTypeByCustomer(Customer);
  
  PrintHead('Detailansicht Kunde');
  PrintHelp(Keys);
  PrintLn;
  PrintLn('Kunden-Nr.: ' + IntToStr(Number));
  PrintLn('Name:       ' + Name);
  PrintLn('Vorname:    ' + FirstName);
  PrintLn('Telefon:    ' + Phone);
  PrintLn;
  PrintLn('Kundengruppe: ' + CustomerType.Name);
  
  if ReadKey() = #0 then
    ReadKey(); // ScanCode lesen und verwerfen
end;

end.

