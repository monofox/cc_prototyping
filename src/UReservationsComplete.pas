unit UReservationsComplete;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UListModel,
  UListView,
  UPaging;

type

TReservationsComplete = class
  public
    FListView:  TListView;
    FPaging:    TPaging;
    
    constructor Create;
    function    GetList: TListModel;
    function    SelectedReservation: Integer;
    procedure   Show;
  
  private
    procedure DrawInterface;
end;

procedure ShowReservationsComplete;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UData,
  UDelete,
  UReservations,
  UReservationEdit,
  UReservationView,
  UInvoice,
  UStringUtils;

constructor TReservationsComplete.Create;
var
  List: TListModel;
begin
  List := Self.GetList;
  Self.FPaging := TPaging.Create(Length(List), 10, false);
  Self.FListView := TListView.Create(Self.FPaging, Columns, List, false);
end;

procedure TReservationsComplete.Show;
var
  KeyCode, ScanCode: Char;
  Redraw: Boolean;
begin
  Redraw := true;
  
  while Redraw do
  begin
    Self.DrawInterface;
    
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        ScanCode := ReadKey;
        case ScanCode of
          #72: // Pfeil hoch
            Self.FPaging.SelectPrev;
          
          #73: // Bild hoch
            Self.FPaging.BrowsePrev;
          
          #75: // Pfeil links
            Self.FPaging.BrowsePrev;
          
          #77: // Pfeil rechts
            Self.FPaging.BrowseNext;
          
          #80: // Pfeil runter
            Self.FPaging.SelectNext;
          
          #81: // Bild runter
            Self.FPaging.BrowseNext;
        end;
      end;
      #13: // Enter
        ReservationView(Self.SelectedReservation);
      
      'd':
      begin
        DeleteReservation(Self.SelectedReservation);
        Self.FListView.FListModel := Self.GetList;
        Self.FPaging.Length := Length(Self.FListView.FListModel);
      end;
      
      'n':
      begin
        ReservationCreate;
        Self.FListView.FListModel := Self.GetList;
        Self.FPaging.Length := Length(Self.FListView.FListModel);
      end;
      
      'e':
      begin
        ReservationEdit(Self.SelectedReservation);
        Self.FListView.FListModel := Self.GetList;
        Self.FPaging.Length := Length(Self.FListView.FListModel);
      end;
      
      'r':
        InvoiceView(Self.SelectedReservation);
      
      #27: // Esc
        Redraw := false;
    end;
  end;
end;

procedure TReservationsComplete.DrawInterface;
begin
  PrintHead('Buchungen');
  PrintHelp(KeysSelect);
  PrintHelp(KeysEdit);
  PrintLn;
  
  Self.FListView.Draw;
  
  Cursor(1, Self.FListView.FSelectedRow);
end;

function TReservationsComplete.GetList: TListModel;
var
  I: Integer;
begin
  SetLength(Result, Length(Data.Reservations));
  for I := 0 to High(Data.Reservations) do
    Result[I] := Data.Reservations[I];
end;

function TReservationsComplete.SelectedReservation: Integer;
begin
	Result := Self.FPaging.SelectedIndex;
end;

procedure ShowReservationsComplete;
var
  ReservationsComplete: TReservationsComplete;
begin
  ReservationsComplete := TReservationsComplete.Create;
  ReservationsComplete.Show;
end;

end.

