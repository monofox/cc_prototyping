unit UData;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  Classes,
  UCamper,
  UCamperClass,
  UCustomer,
  UCustomerType,
  UListModel,
  UReservation;

const
  MAX_WEEKS = 28;

type

TData = class
  private
    constructor Init;
  public
    Campers:           TCampers;
    CamperClasses:     TCamperClasses;
    Customers:         TCustomers;
    CustomerTypes:     TCustomerTypes;
    Reservations:      TReservations;
    LastInvoiceNumber: Integer;
    
    class function Create: TData;
    procedure   AddCamper(Camper: TCamper);
    procedure   AddCamperClass(CamperClass: TCamperClass);
    procedure   AddCustomer(Customer: TCustomer);
    procedure   AddReservation(Reservation: TReservation);
    function    GetCamperByNumber(Number: Integer): Integer;
    function    GetCamperByPlate(Plate: String): Integer;
    function    GetCamperClassByName(Name: String): Integer;
    function    GetCustomerByNumber(Number: Integer): Integer;
    function    GetCustomerTypeByCustomer(Customer: TCustomer): TCustomerType;overload;
    function    GetCustomerTypeByCustomer(Number: Integer): TCustomerType;overload;
    function    ListCampers: TListModel;
    function    ListCamperClasses: TListModel;
    function    ListCamperVariants: TListModel;
    function    ListCustomers: TListModel;
    procedure   LoadConfiguration;
    procedure   UpdateReferenceCamper(OldNumber, NewNumber: Integer);
    procedure   UpdateReferenceCamperClass(StartIndex: Integer);
    procedure   UpdateReferenceCustomer(OldNumber, NewNumber: Integer);
    procedure   Save;
end;

function Data: TData;

function GetResPath: String;

implementation

uses
  SysUtils,
  USerializer;

const
  // Dateien im Programmordner
  DEMOFILE      = 'demo.txt';
  CUSTOMERSFILE = 'customers.txt';
  
  // Ort der Konfigurationsdatei
  FILENAME = 'data.txt';
  {$IFDEF MSWINDOWS}
    CONFIG = 'appdata';
    FOLDER = 'canadiancamper';
  {$ELSE}
    CONFIG = 'HOME';
    FOLDER = '.config/canadiancamper';
  {$ENDIF}

var
  Singleton: TData = nil;


constructor TData.Init;
var
  Path: String;
  ConfigSource: String;
  Data: TStringList;
begin
  // Load configuration
  Self.LoadConfiguration();
  ConfigSource := '';
  
  Path := GetEnvironmentVariable(CONFIG);
  if not DirectoryExists(Path+PathDelim+FOLDER) then
    CreateDir(Path+PathDelim+FOLDER);
  
  if (not FileExists(Path+PathDelim+FOLDER+PathDelim+FILENAME)) and FileExists(GetResPath + DEMOFILE) then
    ConfigSource := GetResPath + DEMOFILE
  else
  begin
    if FileExists(Path+PathDelim+FOLDER+PathDelim+FILENAME) then
      ConfigSource := Path+PathDelim+FOLDER+PathDelim+FILENAME
  end;
  
  if (ConfigSource <> '') then
  begin
    Data := TStringList.Create;
    Data.LoadFromFile(ConfigSource);
    
    LoadCampers      (Data.Values['campers'],       Self.Campers);
    LoadCamperClasses(Data.Values['camperclasses'], Self.CamperClasses);
    LoadCustomers    (Data.Values['customers'],     Self.Customers);
    LoadReservations (Data.Values['reservations'],  Self.Reservations);
    
    try
      Self.LastInvoiceNumber := StrToInt(Data.Values['lastinvoicenumber']);
    except
      on EConvertError do
        Self.LastInvoiceNumber := 0;
    end;
    
    FreeAndNil(Data);
  end
  else
  begin
    SetLength(Self.Campers,       0);
    SetLength(Self.CamperClasses, 0);
    SetLength(Self.Customers,     0);
    SetLength(Self.Reservations,  0);
  end;
  
  LoadCustomerTypes(GetResPath + CUSTOMERSFILE, Self.CustomerTypes);
end;

class function TData.Create: TData;
begin
  raise Exception.Create('Use Data instead of TData.Create');
  Result := nil;// Warnung unterdrücken
end;

procedure TData.AddCamper(Camper: TCamper);
var
  Len: Integer;
begin
  Len := Length(Self.Campers);
  SetLength(Self.Campers, Len + 1);
  Self.Campers[Len] := Camper;
end;

procedure TData.AddCustomer(Customer: TCustomer);
var
  Len: Integer;
begin
  Len := Length(Self.Customers);
  SetLength(Self.Customers, Len + 1);
  Self.Customers[Len] := Customer;
end;

procedure TData.AddReservation(Reservation: TReservation);
var
  Len: Integer;
begin
  Len := Length(Self.Reservations);
  SetLength(Self.Reservations, Len + 1);
  Self.Reservations[Len] := Reservation;
end;

function TData.GetCamperByNumber(Number: Integer): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to High(Self.Campers) do
  begin
    if Self.Campers[I].Number = Number then
    begin
      Result := I;
			break;
    end;
  end;
end;

function TData.GetCamperByPlate(Plate: String): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to High(Self.Campers) do
  begin
    if Self.Campers[I].Plate = Plate then
    begin
      Result := I;
			break;
    end;
  end;
end;

function TData.GetCamperClassByName(Name: String): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to High(Self.CamperClasses) do
  begin
    if Self.CamperClasses[I].Name = Name then
    begin
      Result := I;
			break;
    end;
  end;
end;

function TData.GetCustomerByNumber(Number: Integer): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to High(Self.Customers) do
  begin
    if Self.Customers[I].Number = Number then
    begin
      Result := I;
			break;
    end;
  end;
end;

procedure TData.AddCamperClass(CamperClass: TCamperClass);
var
  Len: Integer;
begin
  Len := Length(Self.CamperClasses);
  SetLength(Self.CamperClasses, Len + 1);
  Self.CamperClasses[Len] := CamperClass;
end;

procedure TData.LoadConfiguration;
begin
  DecimalSeparator := '.';
  ThousandSeparator := ',';
end;

procedure TData.Save;
var
  Path: String;
  Data: TStringList;
begin
  Path := GetEnvironmentVariable(CONFIG);
  if not DirectoryExists(Path+PathDelim+FOLDER) then
    CreateDir(Path+PathDelim+FOLDER);
  
  Data := TStringList.Create;
  
  Data.Values['campers']       := SaveCampers      (Self.Campers);
  Data.Values['camperclasses'] := SaveCamperClasses(Self.CamperClasses);
  Data.Values['customers']     := SaveCustomers    (Self.Customers);
  Data.Values['reservations']  := SaveReservations (Self.Reservations);
  
  Data.Values['lastinvoicenumber'] := IntToStr(Self.LastInvoiceNumber);
  
  Data.SaveToFile(Path+PathDelim+FOLDER+PathDelim+FILENAME);
  FreeAndNil(Data);
end;

function TData.GetCustomerTypeByCustomer(Customer: TCustomer): TCustomerType;
begin
  Result := Self.GetCustomerTypeByCustomer(Customer.Number);
end;

function TData.GetCustomerTypeByCustomer(Number: Integer): TCustomerType;
var
  I: Integer;
begin
  Result := Self.CustomerTypes[0];
  
  for I := 1 to High(Self.CustomerTypes) do
  begin
    if Self.CustomerTypes[I].MinNum <= Number then
      Result := Self.CustomerTypes[I];
  end;
end;

function TData.ListCampers: TListModel;
var
  I: Integer;
begin
  SetLength(Result, Length(Self.Campers));
  for I := 0 to High(Self.Campers) do
    Result[I] := Self.Campers[I];
end;

function TData.ListCamperClasses: TListModel;
var
  I: Integer;
begin
  SetLength(Result, Length(Self.CamperClasses));
  for I := 0 to High(Self.CamperClasses) do
    Result[I] := Self.CamperClasses[I];
end;

function TData.ListCamperVariants: TListModel;
var
  I: Integer;
begin
  SetLength(Result, Integer(High(TCamperVariant)) + 1);
  for I := 0 to Integer(High(TCamperVariant)) do
    Result[I] := TSimpleListRow.Create(GetVariantName(TCamperVariant(I)));
end;

function TData.ListCustomers: TListModel;
var
  I: Integer;
begin
  SetLength(Result, Length(Self.Customers));
  for I := 0 to High(Self.Customers) do
    Result[I] := Self.Customers[I];
end;

procedure TData.UpdateReferenceCamper(OldNumber, NewNumber: Integer);
var
  I: Integer;
begin
  for I := 0 to High(Self.Reservations) do
    if Self.Reservations[I].Camper = OldNumber then
      Self.Reservations[I].Camper := NewNumber;
end;
 
procedure TData.UpdateReferenceCamperClass(StartIndex: Integer);
var
  I: Integer;
begin
  for I := 0 to High(Self.Campers) do
    if Self.Campers[I].CamperClass > StartIndex then
      Self.Campers[I].CamperClass := Self.Campers[I].CamperClass - 1;
end;

procedure TData.UpdateReferenceCustomer(OldNumber, NewNumber: Integer);
var
  I: Integer;
begin
  for I := 0 to High(Self.Reservations) do
    if Self.Reservations[I].Customer = OldNumber then
      Self.Reservations[I].Customer := NewNumber;
end;

function Data: TData;
begin
  Result := Singleton;
end;

function GetResPath: String;
begin
  // Funktioniert nicht, wenn das Programm über einen Symlink ausgeführt wird.
  Result := ExtractFilePath(ParamStr(0));
end;

initialization

Singleton := TData.Init;

end.
