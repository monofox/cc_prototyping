unit UReservations;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UListModel,
  UListView,
  UStringUtils;

type
  TSorterFunc = function(A,B: IListRow): Boolean;

procedure InsertionSort(var List: array of IListRow; Sorter: TSorterFunc);
var
  KeysSelect: array[0..2] of TKey =
  (
    ('Esc', 'Zurück'),
    ('Enter', 'Details'),
    ('r', 'Rechnung')
  );
  KeysEdit: array[0..2] of TKey =
  (
    ('n', 'Neue Buchung'),
    ('e', 'Bearbeiten'),
    ('d', 'Löschen')
  );
  Columns: array[0..6] of TColumn =
  (
    (Name: 'Buchung'; Content: SNumber; Source: 'number';       Length:  7; Padding: true; Decimals: 0),
    (Name: 'Camper';  Content: SNumber; Source: 'camper';       Length:  6; Padding: true; Decimals: 0),
    (Name: 'KNr.';    Content: SNumber; Source: 'customer';     Length:  4; Padding: true; Decimals: 0),
    (Name: 'Name';    Content: SString; Source: 'cu_name';      Length: 20; Padding: true),
    (Name: 'Vorname'; Content: SString; Source: 'cu_firstname'; Length: 20; Padding: true),
    (Name: 'Von';     Content: SNumber; Source: 'startweek';    Length:  3; Padding: false; Decimals: 0),
    (Name: 'Bis';     Content: SNumber; Source: 'endweek';      Length:  3; Padding: false; Decimals: 0)
  );

implementation

procedure InsertionSort(var List: array of IListRow; Sorter: TSorterFunc);
var
  I,J: Integer;
  Tmp: IListRow;
begin
  for I := 1 to High(List) do
  begin
    Tmp := List[I];
    J := I;
    while (J > 0) and Sorter(List[J-1], Tmp) do
    begin
      List[J] := List[J - 1];
      dec(J);
    end;
    List[J] := Tmp;
  end;
end;

end.

