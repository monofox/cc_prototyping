unit UListView;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UPaging,
  UListModel;

type

TColumnType = (SString, SNumber, SBoolean);

TColumn = record
  Name:     String;
  Content:  TColumnType;
  Source:   ShortString;
  Length:   Integer;
  Padding:  Boolean;
  Decimals: Integer;
end;

TColumns = array of TColumn;

TListView = class
  public
    FPaging:      TPaging;
    FListModel:   TListModel;
    FColumns:     array of TColumn;
    FSelectedRow: Integer;
    FSelection:   Boolean;
    
    constructor Create(Paging: TPaging; Columns: array of TColumn; ListModel: TListModel; Selection: Boolean);
    procedure   Draw;
  private
    procedure   PrintBoolean(Val: Boolean; Len: Integer; Padding: Boolean);
    procedure   PrintNumber (Val: Single;  Len: Integer; Padding: Boolean; Decimals: Integer);
    procedure   PrintString (Val: String;  Len: Integer; Padding: Boolean);
end;

implementation

uses
  {$IFNDEF FPC}StrUtils,{$ENDIF}
  SysUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UStringUtils;

constructor TListView.Create(Paging: TPaging; Columns: array of TColumn; ListModel: TListModel; Selection: Boolean);
var
  I: Integer;
begin
  Self.FPaging    := Paging;
  Self.FListModel := ListModel;
  Self.FSelection := Selection;
  
  // Direktes Zuweisen ist nicht möglich
  SetLength(Self.FColumns, Length(Columns));
  for I := 0 to High(Columns) do
    Self.FColumns[I] := Columns[I];
end;

procedure TListView.Draw;
var
  I, J, Count, FirstRow: Integer;
  Row: IListRow;
begin
  if Self.FPaging.MaxPage > 0 then
  begin
    Cursor(2, WhereY);
    Print('Zeige %3d von %3d Einträgen',
    [
      Self.FPaging.EndIndex - Self.FPaging.StartIndex + 1,
      Self.FPaging.Length
    ]);
    Cursor(65, WhereY);
    Print('Seite %2d von %2d',
    [
      Self.FPaging.Page + 1,
      Self.FPaging.MaxPage + 1
    ]);
  end
  else
  begin
    Cursor(2, WhereY);
    if Self.FPaging.Length = 1 then
      Print('Zeige 1 Eintrag')
    else
      Print('Zeige %d Einträge', [Self.FPaging.Length]);
  end;
  
  if Self.FSelection then
  begin
    Count := Self.FPaging.CountSelected;
    Cursor(36, WhereY);
    if Count = 1 then
      Print('  1 Eintrag markiert')
    else
      Print('%3d Einträge markiert', [Count]);
  end;
  
  PrintLn;
  PrintLn;
  
  if Self.FSelection then
    Print('  ');
  for I := 0 to High(Self.FColumns) do
  begin
    if I <> 0 then
      Print('|');
    Self.PrintString(Self.FColumns[I].Name, Self.FColumns[I].Length, Self.FColumns[I].Padding);
  end;
  PrintLn;
  
  if Self.FSelection then
    Print('--');
  
  for I := 0 to High(Self.FColumns) do
  begin
    if I <> 0 then
      Print('+');
    
    Print(RepeatChar('-', Self.FColumns[I].Length));
    if Self.FColumns[I].Padding then
      Print('--');
  end;
  PrintLn;
  
  FirstRow := WhereY;
  
  for I := 0 to Self.FPaging.EndIndex-Self.FPaging.StartIndex do
  begin
    Row := Self.FListModel[I+Self.FPaging.StartIndex];
    
    Cursor(1, FirstRow+I);
    ClrEol;
    
    if Self.FSelection then
    begin
      if Self.FPaging.IsSelected(I + Self.FPaging.StartIndex) then
        Print('☒ ')
      else
        Print('☐ ');
    end;
    
    
    for J := 0 to High(Self.FColumns) do
    begin
      if J <> 0 then
        Print('|');
      
      case Self.FColumns[J].Content of
        SString:   Self.PrintString (Row.GetString (Self.FColumns[J].Source),  Self.FColumns[J].Length, Self.FColumns[J].Padding);
        SNumber:   Self.PrintNumber (Row.GetNumber (Self.FColumns[J].Source),  Self.FColumns[J].Length, Self.FColumns[J].Padding, Self.FColumns[J].Decimals);
        SBoolean:  Self.PrintBoolean(Row.GetBoolean(Self.FColumns[J].Source), Self.FColumns[J].Length, Self.FColumns[J].Padding);
      end;
    end;
  end;
  
  Self.FSelectedRow := FirstRow+Self.FPaging.SelectedIndex-Self.FPaging.StartIndex;
end;

procedure TListView.PrintString(Val: String; Len: Integer; Padding: Boolean);
var
  Content: String;
begin
  if Padding then
  begin
    Content := ' ' + LeftStr(Val, Len);
    if Length(Val) > Len then
      Content := Content + '…';
    
    Content := Content + StringOfChar(' ', Len + 2 - Length(Content));
  end
  else
  begin
    if Length(Val) > Len then
      Content := LeftStr(Val, Len-1) + '…'
    else
      Content := LeftStr(Val, Len);
    
    Content := Content + StringOfChar(' ', Len - Length(Content));
  end;
  Print(Content);
end;

procedure TListView.PrintNumber(Val: Single; Len: Integer; Padding: Boolean; Decimals: Integer);
var
  Content: String;
begin
  Content := Format('%' + IntToStr(Len) + '.' + IntToStr(Decimals) + 'f', [Val]);
  
  if Padding then
  begin
    if Length(Content) > Len then
      Content := '…' + RightStr(Content, Len) + ' '
    else
      Content := ' ' + StringOfChar(' ', Len - Length(Content)) + Content + ' ';
  end
  else
    if Length(Content) > Len then
      Content := '…' + RightStr(Content, Len-1);
  
  Print(Content);
end;

procedure TListView.PrintBoolean(Val: Boolean; Len: Integer; Padding: Boolean);
var
  Content: String;
begin
  if Val then
    Content := '☑ '
  else
    Content := '☐ ';
  Content := StringOfChar(' ', Len div 2 ) + Content;
  if Padding then
    Content := ' ' + Content + StringOfChar(' ', Len - Length(Content) + 1)
  else
    Content := Content + StringOfChar(' ', Len - Length(Content));
  Print(Content);
end;

end.

