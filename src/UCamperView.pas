unit UCamperView;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UPaging;

procedure CamperView(Paging: TPaging);
procedure CamperViewInterface(CamperNr: Integer);

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UCamper,
  UCamperClass,
  UData,
  UReserved,
  UStringUtils;

var
  Keys: array[0..0] of TKey = (('Beliebige Taste', 'Zurück'));

procedure CamperView(Paging: TPaging);
var
  Count, I: Integer;
begin
  
  Count := Paging.CountSelected;
  if Count = 0 then
  begin
    CamperViewInterface(Paging.SelectedIndex);
  end
  else
  begin
    for I := Paging.Length - 1 downto 0 do
    begin
      if not Paging.IsSelected(I) then
        continue;
         
      CamperViewInterface(I);
      Paging.Unselect(I);
    end;
  end;
  
  Paging.Length := Length(Data.Campers);
end;

procedure CamperViewInterface(CamperNr: Integer);
var
  Model:  String;
  Number: Integer;
  Plate:  String;
  Camper: TCamper;
begin
  // We have to load the existent data for the camper.
  Camper := Data.Campers[CamperNr];
  Number := Camper.Number;
  Plate  := Camper.Plate;
  Model  := Data.CamperClasses[Camper.CamperClass].Name;
  
  PrintHead('Detailansicht Camper');
  PrintHelp(Keys);
  PrintLn;
  PrintLn('Stellplatz:  %d', [Number]);
  PrintLn('Kennzeichen: ' + Plate);
  PrintLn('Modell:      ' + Model);
  PrintLn('Preis:       %1.2f $', [Data.CamperClasses[Camper.CamperClass].Price]);
  PrintLn('Hubraum:     %1.1f l', [Data.CamperClasses[Camper.CamperClass].Engine]);
  PrintLn('Leistung:    %1d PS',  [Data.CamperClasses[Camper.CamperClass].Power]);
  PrintLn;
  PrintLn;
  ReservedBarForCamperNumber(Camper.Number);
  
  if ReadKey() = #0 then
    ReadKey(); // ScanCode lesen und verwerfen
end;

end.

