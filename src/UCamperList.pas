unit UCamperList;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UPaging,
  UListView;

type

TCamperList = class
  public
    FListView: TListView;
    FPaging:   TPaging;
    
    constructor Create;
    procedure   Show;
  
  private
    procedure DrawInterface;
end;

procedure ShowCamperList;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UCamperEdit,
  UCamperView,
  UData,
  UDelete,
  UStringUtils;

var
  KeysSelect: array[0..2] of TKey =
  (
    ('Leertaste', 'Auswählen'),
    ('Esc', 'Zurück'),
    ('Enter', 'Details')
  );
  KeysEdit: array[0..2] of TKey =
  (
    ('n', 'Neuer Camper'),
    ('e', 'Bearbeiten'),
    ('d', 'Löschen')
  );
  KeysBack: array[0..0] of TKey =
  (
    ('Enter, Esc', 'Zurück')
  );
  KeysCreate: array[0..1] of TKey =
  (
    ('Esc', 'Zurück'),
    ('Enter', 'Camper anlegen')
  );
  Columns: array[0..4] of TColumn =
  (
    (Name: 'Stellplatz';  Content: SNumber; Source: 'number'; Length: 10; Padding: true; Decimals: 0),
    (Name: 'Modell';      Content: SString; Source: 'class';  Length: 10; Padding: true),
    (Name: 'Kennzeichen'; Content: SString; Source: 'plate';  Length: 11; Padding: true),
    (Name: 'Leistung';    Content: SNumber; Source: 'power';  Length:  8; Padding: true; Decimals: 0),
    (Name: 'Hubraum';     Content: SNumber; Source: 'engine'; Length:  7; Padding: true; Decimals: 1)
  );

constructor TCamperList.Create;
begin
  Self.FPaging := TPaging.Create(Length(Data.Campers), 10, true);
  Self.FListView := TListView.Create(Self.FPaging, Columns, Data.ListCampers, true);
end;

procedure TCamperList.Show;
var
  KeyCode, ScanCode: Char;
  Redraw: Boolean;
begin
  Redraw := true;
  
  while Redraw do
  begin
    Self.DrawInterface;
    
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        ScanCode := ReadKey;
        case ScanCode of
          #72: // Pfeil hoch
            Self.FPaging.SelectPrev;
          
          #73: // Bild hoch
            Self.FPaging.BrowsePrev;
          
          #75: // Pfeil links
            Self.FPaging.BrowsePrev;
          
          #77: // Pfeil rechts
            Self.FPaging.BrowseNext;
          
          #80: // Pfeil runter
            Self.FPaging.SelectNext;
          
          #81: // Bild runter
            Self.FPaging.BrowseNext;
        end;
      end;

      #13: // Enter
        CamperView(Self.FPaging);
      
      ' ':
      begin
        if Self.FPaging.IsSelected(Self.FPaging.SelectedIndex) then
          Self.FPaging.Unselect(Self.FPaging.SelectedIndex)
        else
          Self.FPaging.Select(Self.FPaging.SelectedIndex);
      end;
      
      'd':
      begin
        DeleteCamper(Self.FPaging);
        Self.FListView.FListModel := Data.ListCampers;
        if Self.FPaging.Length = 0 then
          Redraw := false;
      end;
      
      'n':
      begin
        CamperCreate;
        Self.FListView.FListModel := Data.ListCampers;
        Self.FPaging.Length := Length(Data.Campers);
      end;
      
      'e':
      begin
        CamperEdit(Self.FPaging);
        Self.FListView.FListModel := Data.ListCampers;
        Self.FPaging.Length := Length(Data.Campers);
      end;
      
      #27: // Esc
        Redraw := false;
    end;
  end;
end;

procedure TCamperList.DrawInterface;
begin
  PrintHead('Übersicht Camper');
  PrintHelp(KeysSelect);
  PrintHelp(KeysEdit);
  PrintLn;
  
  Self.FListView.Draw;
  
  Cursor(1, Self.FListView.FSelectedRow);
end;

procedure ShowCamperList;
var
  CamperList: TCamperList;
begin
  if Length(Data.Campers) = 0 then
  begin
    PrintHead('Übersicht Camper');
    if Length(Data.CamperClasses) = 0 then
    begin
      PrintHelp(KeysBack);
      PrintLn;
      PrintLn('Es sind noch keine Camper eingetragen.');
      PrintLn;
      PrintLn('Um einen neuen Camper anzulegen,');
      PrintLn('müssen Sie zunächst einen Camper-Typ definieren.');
      
      while true do
      begin
        case ReadKey of
          #0:
            ReadKey;//ScanCode lesen und verwerfen
          
          #13, #27:
            break;
        end;
      end;
    end
    else
    begin
      PrintHelp(KeysCreate);
      PrintLn;
      PrintLn('Es sind noch keine Camper eingetragen.');
      while true do
      begin
        case ReadKey of
          #0:
            ReadKey;//ScanCode lesen und verwerfen
          
          #13:
          begin
            CamperCreate;
            break;
          end;
          
          #27:
            break;
        end;
      end;
    end;
  end;
  
  if Length(Data.Campers) > 0 then
  begin
    CamperList := TCamperList.Create;
    CamperList.Show;
  end;
end;

end.

