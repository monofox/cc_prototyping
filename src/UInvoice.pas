unit UInvoice;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

function InvoiceViewByNumber(InvoiceNumber: Integer): Boolean;
procedure InvoiceView(ReservationIndex: Integer);

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UCamper,
  UCamperClass,
  UCustomer,
  UCustomerType,
  UData,
  UReservation,
  UStringUtils;

var
  Keys: array[0..0] of TKey = (('Beliebige Taste', 'Zurück'));

function InvoiceViewByNumber(InvoiceNumber: Integer): Boolean;
var
  I: Integer;
begin
  Result := false;
  for I := 0 to High(Data.Reservations) do
  begin
    if Data.Reservations[I].Number = InvoiceNumber then
    begin
      InvoiceView(I);
      Result := true;
      break;
    end;
  end;
end;

procedure InvoiceView(ReservationIndex: Integer); 
var
  Camper:       TCamper;
  CamperClass:  TCamperClass;
  Customer:     TCustomer;
  CustomerType: TCustomerType;
  Reservation:  TReservation;
  RegularPrice: Single;
  ReducedPrice: Single;
begin
  Reservation := Data.Reservations[ReservationIndex];
  
  Camper       := Data.Campers[Data.GetCamperByNumber(Reservation.Camper)];
  CamperClass  := Data.CamperClasses[Camper.CamperClass];
  Customer     := Data.Customers[Data.GetCustomerByNumber(Reservation.Customer)];
  CustomerType := Data.GetCustomerTypeByCustomer(Customer);
  
  PrintHead('Rechnung');
  PrintHelp(Keys);
  PrintLn;
  PrintLn('Rechnungsnummer: %d', [Reservation.Number]);
  PrintLn;
  PrintLn('Kunde:');
  PrintLn('  Kundennummer: %d',[Customer.Number]);
  PrintLn('  Name:         %s', [Customer.Name]);
  PrintLn('  Vorname:      %s', [Customer.FirstName]);
  PrintLn('  Telefon:      %s', [Customer.Phone]);
  PrintLn('  Kundengruppe: %s', [CustomerType.Name]);
  PrintLn('  Rabatt:       %1.2f %%', [CustomerType.Discount]);
  PrintLn;
  PrintLn('Camper:');
  PrintLn('  Stellplatz:   %d', [Camper.Number]);
  PrintLn('  Kennzeichen:  %s', [Camper.Plate]);
  PrintLn('  Typ:          %s', [CamperClass.Name]);
  PrintLn('  Plätze:       %d', [CamperClass.Size]);
  PrintLn('  Ausführung:   %s', [GetVariantName(CamperClass.Variant)]);
  PrintLn;
  PrintLn('Von Woche:     %2d', [Reservation.StartWeek]);
  PrintLn('Bis Woche:     %2d', [Reservation.EndWeek]);
  PrintLn('Anzahl Wochen: %2d', [Reservation.EndWeek - Reservation.StartWeek]);
  PrintLn;

  RegularPrice := CamperClass.Price;
  ReducedPrice := CamperClass.Price - (CamperClass.Price*(CustomerType.Discount/100));
  PrintLn('Regulärer Preis/Woche:  %10.2f $', [CamperClass.Price]);
  PrintLn('Ermäßigter Preis/Woche: %10.2f $', [ReducedPrice]);
  PrintLn;
  RegularPrice := (Reservation.EndWeek - Reservation.StartWeek) * RegularPrice;
  ReducedPrice := (Reservation.EndWeek - Reservation.StartWeek) * ReducedPrice;
  PrintLn('Regulärer Preis:        %10.2f $', [RegularPrice]);
  PrintLn('Ermäßigter Preis:       %10.2f $', [ReducedPrice]);

  
  if ReadKey() = #0 then
    ReadKey(); // ScanCode lesen und verwerfen
end;

end.

