unit UReservationView;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

procedure ReservationView(Index: Integer);

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UCamper,
  UCamperClass,
  UCustomer,
  UCustomerType,
  UData,
  UReservation,
  UStringUtils;

var
  Keys: array[0..0] of TKey = (('Beliebige Taste', 'Zurück'));

procedure ReservationView(Index: Integer); 
var
  Camper:       TCamper;
  CamperClass:  TCamperClass;
  Customer:     TCustomer;
  CustomerType: TCustomerType;
  Reservation:  TReservation;
begin
  Reservation := Data.Reservations[Index];
  
  Camper       := Data.Campers[Data.GetCamperByNumber(Reservation.Camper)];
  CamperClass  := Data.CamperClasses[Camper.CamperClass];
  Customer     := Data.Customers[Data.GetCustomerByNumber(Reservation.Customer)];
  CustomerType := Data.GetCustomerTypeByCustomer(Customer);
  
  PrintHead('Detailansicht Buchung');
  PrintHelp(Keys);
  PrintLn;
  PrintLn('Von Woche: %2d',    [Reservation.StartWeek]);
  PrintLn('Bis Woche: %2d',    [Reservation.EndWeek]);
  PrintLn;
  PrintLn('Kunde:');
  PrintLn('  Kundennummer: %d',[Customer.Number]);
  PrintLn('  Name:         %s', [Customer.Name]);
  PrintLn('  Vorname:      %s', [Customer.FirstName]);
  PrintLn('  Telefon:      %s', [Customer.Phone]);
  PrintLn('  Kundengruppe: %s', [CustomerType.Name]);
  PrintLn;
  PrintLn('Camper:');
  PrintLn('  Stellplatz:   %d', [Camper.Number]);
  PrintLn('  Kennzeichen:  %s', [Camper.Plate]);
  PrintLn('  Typ:          %s', [CamperClass.Name]);
  PrintLn('  Plätze:       %d', [CamperClass.Size]);
  PrintLn('  Ausführung:   %s', [GetVariantName(CamperClass.Variant)]);
  PrintLn;
  
  if ReadKey() = #0 then
    ReadKey(); // ScanCode lesen und verwerfen
end;

end.

