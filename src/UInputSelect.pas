unit UInputSelect;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UInput,
  UListModel,
  UListView,
  UPaging;

type

TInputSelect = class
  private
    FActive:     Boolean;
    FListModel:  TListModel;
    FListView:   TListView;
    FPaging:     TPaging;
    FPosX:       Integer;
    FPosY:       Integer;
    FSelected:   Integer;
    FShowWindow: Boolean;
    FTitle:      String;
    
    procedure    WriteValue(AValue: Integer);
  
  public
    property     Value: Integer read FSelected write WriteValue;
    
    constructor  Create(Title: String; Columns: array of TColumn; ListModel: TListModel; StartOpt: Integer);
    function     Activate: TInputReturnKey;
    procedure    Draw;
    procedure    DrawActive;
    procedure    DrawPassive;
    procedure    SetPosition;
end;

implementation

uses
  SysUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UStringUtils;

constructor TInputSelect.Create(Title: String; Columns: array of TColumn; ListModel: TListModel; StartOpt: Integer);
begin
  Self.FActive      := false;
  Self.FShowWindow  := false;
  Self.FListModel   := ListModel;
  Self.FTitle       := Title;
  if (StartOpt < 0) or (StartOpt > High(ListModel)) then
    StartOpt        := 0;
  Self.FSelected    := StartOpt;
  
  Self.FPaging := TPaging.Create(Length(ListModel), 15, false);
  Self.FPaging.SelectedIndex := StartOpt;
  
  Self.FListView := TListView.Create(Self.FPaging, Columns, ListModel, false);
  
  Self.SetPosition;
end;

function TInputSelect.Activate: TInputReturnKey;
var
  Action: TInputAction;
  KeyCode: Char;
begin
  Self.FActive := true;
  Self.FShowWindow := false;
  Result := SEscape;
  Action := SCont;
  
  repeat
  begin
    Self.Draw;
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        KeyCode := ReadKey;
        case KeyCode of
          #71: // Numpad Pos1
            Self.FPaging.SelectedIndex := 0;
          
          #72: // Pfeil hoch
            Self.FPaging.SelectPrev;
          
          #73: // Bild hoch
            Self.FPaging.BrowsePrev;
          
          #75: // Pfeil links
            Self.FPaging.BrowsePrev;
          
          #77: // Pfeil rechts
            Self.FPaging.BrowseNext;
          
          #80: // Pfeil runter
            Self.FPaging.SelectNext;
          
          #81: // Bild runter
            Self.FPaging.BrowseNext;
          
          #79: // Numpad Ende
            Self.FPaging.SelectedIndex := High(Self.FListModel);
        end;
      end;
      
      #9: // Tab
      begin
        Action := SSave;
        Result := STab;
      end;
      
      #13: // Enter
      begin
        Action := SSave;
        Result := SReturn;
      end;
      
      #27: // Esc
      begin
        if Self.FShowWindow then
          Result := SReturn
        else
          Result := SEscape;
        Action := SDiscard;
      end;

      ' ': // Space
      begin
        Self.FShowWindow := true;
      end;
    end;
  end;
  until Action <> SCont;
  
  Self.FActive := false;
  Self.FShowWindow := false;
  
  if Action = SSave then
    begin
    Self.FSelected := Self.FPaging.SelectedIndex;
    end
  else
    begin
    Self.FPaging.SelectedIndex := Self.FSelected;
  end;
end;

procedure TInputSelect.Draw;
begin
  if Self.FActive and Self.FShowWindow then
    Self.DrawActive
  else
    Self.DrawPassive;
end;

procedure TInputSelect.DrawPassive;
var
  X, Y: Integer;
  VisibleStr: AnsiString;
begin
  X := WhereX;
  Y := WhereY;
  Cursor(Self.FPosX, Self.FPosY);
  
  if Self.FActive then
  begin
    TextColor(Black);
    TextBackground(White);
  end;
  
  VisibleStr := Self.FListModel[Self.FPaging.SelectedIndex].GetString(Self.FListView.FColumns[0].Source);
  VisibleStr := Copy(VisibleStr, 1, Self.FListView.FColumns[0].Length);
  Print('%-'+IntToStr(Self.FListView.FColumns[0].Length)+'s', [VisibleStr]);
  
  if not Self.FActive then
    Print(' ')
  else
  begin
    TextColor(Red);
    Print('▼');
  end;
  NormVideo;
  
  if Self.FActive then
  begin
    Cursor(Self.FPosX + Self.FListView.FColumns[0].Length, Self.FPosY);
  end
  else
    Cursor(X, Y);
end;

procedure TInputSelect.DrawActive;
begin
  PrintHead(Self.FTitle);
  PrintLn;
  
  Self.FListView.Draw;
  Cursor(1, Self.FListView.FSelectedRow);
end;

procedure TInputSelect.WriteValue(AValue: Integer);
begin
  Self.FPaging.SelectedIndex := AValue;
end;

procedure TInputSelect.SetPosition;
begin
  Self.FPosX := WhereX;
  Self.FPosY := WhereY;
end;

end.

