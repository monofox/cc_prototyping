unit UInputButton;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UInput;

type

TInputButton = class
  private
    FActive:      Boolean;
    FLabel:       String;
    FPosX:        Integer;
    FPosY:        Integer;
  
  public
    constructor Create(Lbl: String);
    function    Activate: TInputReturnKey;
    procedure   Draw;
    procedure   SetPosition;
end;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UStringUtils;

constructor TInputButton.Create(Lbl: String);
begin
  Self.SetPosition;
  
  Self.FLabel := Lbl;
end;

function TInputButton.Activate: TInputReturnKey;
var
  X,Y: Integer;
  KeyCode: Char;
begin
  X := WhereX;
  Y := WhereY;
  
  Cursor(Self.FPosX, Self.FPosY);
  
  Self.FActive := true;
  
  Result := SNone;
  
  Self.Draw;
  
  repeat
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
        ReadKey;
      
      #9: // Tab
        Result := STab;
      
      #13: // Enter
        Result := SReturn;
      
      #27: // Esc
        Result := SEscape;
      
      #32: // Space
        Result := SSpace;
    end;
  until Result <> SNone;
  
  Self.FActive := false;
  Self.Draw;
  Cursor(X, Y);
end;

procedure TInputButton.Draw();
var
  X,Y: Integer;
begin
  X := WhereX;
  Y := WhereY;
  
  Cursor(Self.FPosX, Self.FPosY);
  
  if Self.FActive then
  begin
    TextBackground(White);
    TextColor(Red);
  end;
  
  Print('[ %s ]', [Self.FLabel]);
  
  NormVideo;
  Cursor(X, Y);
end;

procedure TInputButton.SetPosition;
begin
  Self.FPosX := WhereX;
  Self.FPosY := WhereY;
end;

end.

