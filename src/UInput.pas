unit UInput;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

type

TInputReturnKey = (STab, SReturn, SEscape, SSpace, SNone);

TInputAction = (SSave, SDiscard, SCont);

implementation

end.

