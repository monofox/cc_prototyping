unit UReservation;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UListModel;

type

TReservation = class(TInterfacedObject, IListRow)
  public
    Customer, Camper, StartWeek, EndWeek, Number: Integer;
    
    constructor Create(ACustomer, ACamper, AStartWeek, AEndWeek: Integer);
    constructor Deserialize(Data: AnsiString);
    function    GetBoolean(Column: ShortString): Boolean;
    function    GetNumber (Column: ShortString): Single;
    function    GetString (Column: ShortString): String;
    function    Serialize: AnsiString;
end;

TReservations = array of TReservation;

implementation

uses
  Classes,
  SysUtils,
  UData;

constructor TReservation.Create(ACustomer, ACamper, AStartWeek, AEndWeek: Integer);
begin
  Self.Customer  := ACustomer;
  Self.Camper    := ACamper;
  Self.StartWeek := AStartWeek;
  Self.EndWeek   := AEndWeek;

  Data.LastInvoiceNumber := Data.LastInvoiceNumber + 1;
  Self.Number := Data.LastInvoiceNumber;
end;

constructor TReservation.Deserialize(Data: AnsiString);
var
  Container: TStringList;
begin
  Container := TStringList.Create;
  Container.DelimitedText := Data;

  Self.Customer  := StrToInt(Container.Values['customer']);
  Self.Camper    := StrToInt(Container.Values['camper']);
  Self.StartWeek := StrToInt(Container.Values['startweek']);
  Self.EndWeek   := StrToInt(Container.Values['endweek']);
  Self.Number    := StrToInt(Container.Values['number']);

  FreeAndNil(Container);
end;

function TReservation.Serialize(): AnsiString;
var
  Container: TStringList;
begin
  Container := TStringList.Create();

  Container.Values['customer']  := IntToStr(Self.Customer);
  Container.Values['camper']    := IntToStr(Self.Camper);
  Container.Values['startweek'] := IntToStr(Self.StartWeek);
  Container.Values['endweek']   := IntToStr(Self.EndWeek);
  Container.Values['number']    := IntToStr(Self.Number);

  Result := Container.DelimitedText;
  
  FreeAndNil(Container);
end;

function TReservation.GetString(Column: ShortString): String;
begin
  Result := '';
  if Column = 'cu_name' then
    Result := Data.Customers[Data.GetCustomerByNumber(Self.Customer)].Name;
  if Column = 'cu_firstname' then
    Result := Data.Customers[Data.GetCustomerByNumber(Self.Customer)].FirstName;
end;

function TReservation.GetNumber(Column: ShortString): Single;
var
  I: Integer;
begin
  Result := 0;
  if Column = 'customer' then
    Result := Self.Customer;
  if Column = 'number' then
    Result := Self.Number;
  if Column = 'camper' then
    Result := Self.Camper;
  if Column = 'startweek' then
    Result := Self.StartWeek;
  if Column = 'endweek' then
    Result := Self.EndWeek;
  if Column = 'index' then
  begin
    for I := 0 to High(Data.Reservations) do
    begin
      if Data.Reservations[I] = Self then
      begin
        Result := I;
        break
      end;
    end;
  end;
end;

function TReservation.GetBoolean(Column: ShortString): Boolean;
begin
  Result := false;
end;

end.
