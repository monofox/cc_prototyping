unit UReserved;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UCamper;

procedure ReservedBarForCamper(Camper: TCamper);
procedure ReservedBarForCamperIndex(Number: Integer);
procedure ReservedBarForCamperNumber(Number: Integer);

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UData,
  UStringUtils;

procedure ReservedBarForCamper(Camper: TCamper);
begin
  ReservedBarForCamperNumber(Camper.Number);
end;

procedure ReservedBarForCamperIndex(Number: Integer);
begin
  ReservedBarForCamper(Data.Campers[Number]);
end;

procedure ReservedBarForCamperNumber(Number: Integer);
var
  I,J: Integer;
  Reserved: array[1..MAX_WEEKS] of Boolean;
begin
  for I := 1 to MAX_WEEKS do
    Reserved[I] := false;
  
  for I := 0 to High(Data.Reservations) do
  begin
    if Data.Reservations[I].Camper = Number then
      for J := Data.Reservations[I].StartWeek to Data.Reservations[I].EndWeek-1 do
        Reserved[J] := true;
  end;
  
  PrintLn('  Rervierungsstatus für den gewählten Camper:');
  Print('  ');
  for I := 1 to MAX_WEEKS do
  begin
    if (I = 1) or (I = MAX_WEEKS) or (I mod 5 = 0) then
      Print('%0.02d', [I])
    else
      Print('  ');
  end;
  PrintLn;
  
  Print('  ');
  for I := 1 to MAX_WEEKS do
  begin
    if Reserved[I] then
      TextBackground(Red)
    else
      TextBackground(Green);
    Print('[]');
  end;
  NormVideo;
  PrintLn;
end;

end.

