unit UCustomerList;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UPaging,
  UListView;

type

TCustomerList = class
  public
    FListView: TListView;
    FPaging:   TPaging;
    
    constructor Create;
    procedure Show;
  
  private
    procedure DrawInterface;
end;

procedure ShowCustomerList;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UCustomerEdit,
  UCustomerView,
  UData,
  UDelete,
  UStringUtils;

var
  KeysSelect: array[0..2] of TKey =
  (
    ('Leertaste', 'Auswählen'),
    ('Esc', 'Zurück'),
    ('Enter', 'Details')
  );
  KeysEdit: array[0..2] of TKey =
  (
    ('n', 'Neuer Kunde'),
    ('e', 'Bearbeiten'),
    ('d', 'Löschen')
  );
  KeysCreate: array[0..1] of TKey =
  (
    ('Esc', 'Zurück'),
    ('Enter', 'Kunde anlegen')
  );
  Columns: array[0..4] of TColumn =
  (
    (Name: 'Nr.';          Content: SNumber; Source: 'number';    Length:  4; Padding: false; Decimals: 0),
    (Name: 'Name';         Content: SString; Source: 'name';      Length: 20; Padding: true),
    (Name: 'Vorname';      Content: SString; Source: 'firstname'; Length: 20; Padding: true),
    (Name: 'Telefon';      Content: SString; Source: 'phone';     Length: 13; Padding: false),
    (Name: 'Kundengruppe'; Content: SString; Source: 'group';     Length: 12; Padding: false)
  );

constructor TCustomerList.Create;
begin
  Self.FPaging := TPaging.Create(Length(Data.Customers), 10, true);
  Self.FListView := TListView.Create(Self.FPaging, Columns, Data.ListCustomers, true);
end;

procedure TCustomerList.Show;
var
  KeyCode, ScanCode: Char;
  Redraw: Boolean;
begin
  Redraw := true;
  
  while Redraw do
  begin
    Self.DrawInterface;
    
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        ScanCode := ReadKey;
        case ScanCode of
          #72: // Pfeil hoch
            Self.FPaging.SelectPrev;
          
          #73: // Bild hoch
            Self.FPaging.BrowsePrev;
          
          #75: // Pfeil links
            Self.FPaging.BrowsePrev;
          
          #77: // Pfeil rechts
            Self.FPaging.BrowseNext;
          
          #80: // Pfeil runter
            Self.FPaging.SelectNext;
          
          #81: // Bild runter
            Self.FPaging.BrowseNext;
        end;
      end;

      #13: // Enter
        CustomerView(Self.FPaging);
      
      ' ':
      begin
        if Self.FPaging.IsSelected(Self.FPaging.SelectedIndex) then
          Self.FPaging.Unselect(Self.FPaging.SelectedIndex)
        else
          Self.FPaging.Select(Self.FPaging.SelectedIndex);
      end;
      
      'd':
      begin
        DeleteCustomer(Self.FPaging);
        Self.FListView.FListModel := Data.ListCustomers;
        if Self.FPaging.Length = 0 then
          Redraw := false;
      end;
      
      'n':
      begin
        CustomerCreate;
        Self.FPaging.Length := Length(Data.Customers);
        Self.FListView.FListModel := Data.ListCustomers;
      end;
      
      'e':
      begin
        CustomerEdit(Self.FPaging);
        Self.FPaging.Length := Length(Data.Customers);
        Self.FListView.FListModel := Data.ListCustomers;
      end;
      
      #27: // Esc
        Redraw := false;
    end;
  end;
end;

procedure TCustomerList.DrawInterface;
begin
  PrintHead('Übersicht Kunden');
  PrintHelp(KeysSelect);
  PrintHelp(KeysEdit);
  PrintLn;
  
  Self.FListView.Draw;
  
  Cursor(1, Self.FListView.FSelectedRow);
end;

procedure ShowCustomerList;
var
  CustomerList: TCustomerList;
begin
  if Length(Data.Customers) = 0 then
  begin
    PrintHead('Übersicht Kunden');
    PrintHelp(KeysCreate);
    PrintLn;
    PrintLn('Es sind noch keine Kunden eingetragen.');
    while true do
    begin
      case ReadKey of
        #0:
          ReadKey;
        
        #13:
        begin
          CustomerCreate;
          break;
        end;
        
        #27:
          break;
      end;
    end;
  end;
  
  if Length(Data.Customers) > 0 then
  begin
    CustomerList := TCustomerList.Create;
    CustomerList.Show;
  end;
end;

end.

