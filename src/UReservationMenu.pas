unit UReservationMenu;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UInputNumber,
  UInputSelect;

type

TReservationMenu = class
  private
    FSelectView:     TInputSelect;
    FSelectCamper:   TInputSelect;
    FSelectCustomer: TInputSelect;
    FSelectWeek:     TInputNumber;
    FInvoiceNumber:  TInputNumber;
  
  public
    constructor Create;
    procedure   Draw;
    procedure   Show;
end;

procedure ShowReservationMenu;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UData,
  UInput,
  UListModel,
  UListView,
  UPaging,
  UReservationEdit,
  UReservationsCamper,
  UReservationsComplete,
  UReservationsCustomer,
  UReservationsWeek,
  UInvoice,
  UStringUtils;

var
  SelectViewModel: TListModel;
  SelectViewColumns: array[0..0] of TColumn =
  (
    (Name: 'Ansicht'; Content: SString; Source: 'label'; Length: 21; Padding: true)
  );
  SelectCustomerColumns: array[0..1] of TColumn =
  (
    (Name: 'Kunde';        Content: SString; Source: 'fullname'; Length: 30; Padding: true),
    (Name: 'Kundennummer'; Content: SNumber; Source: 'number';   Length: 12; Padding: true; Decimals: 0)
  );
  SelectCamperColumns: array[0..2] of TColumn =
  (
    (Name: 'Stellplatz';  Content: SNumber; Source: 'number'; Length: 10; Padding: true; Decimals: 0),
    (Name: 'Modell';      Content: SString; Source: 'class';  Length: 10; Padding: true),
    (Name: 'Kennzeichen'; Content: SString; Source: 'plate';  Length: 11; Padding: true)
  );
  Keys: array[0..2] of TKey = 
  (
    ('Tab',   'Zum nächsten Feld'),
    ('Enter', 'Bestätigen'),
    ('Esc',   'Zurück')
  );
  KeysBack: array[0..0] of TKey =
  (
    ('Enter, Esc', 'Zurück')
  );

constructor TReservationMenu.Create;
begin
  Self.FSelectView     := TInputSelect.Create('Ansicht auswählen', SelectViewColumns,     SelectViewModel, 0);
  Self.FSelectCamper   := TInputSelect.Create('Camper auswählen',  SelectCamperColumns,   Data.ListCampers, 0);
  Self.FSelectCustomer := TInputSelect.Create('Kunde auswählen',   SelectCustomerColumns, Data.ListCustomers, 0);
  Self.FSelectWeek     := TInputNumber.Create(1, 1, MAX_WEEKS, true);
  Self.FInvoiceNumber  := TInputNumber.create(1, 1, Data.LastInvoiceNumber, true);
end;

procedure TReservationMenu.Draw;
begin
  PrintHead('Buchungen');
  PrintHelp(Keys);
  PrintLn;
  
  Print('Ansicht auswählen: ');
  Self.FSelectView.SetPosition;
  Self.FSelectView.Draw;
  PrintLn;
  
  case Self.FSelectView.Value of
    2:
    begin
      Print('Camper auswählen:  ');
      Self.FSelectCamper.SetPosition;
      Self.FSelectCamper.Draw;
    end;
    3:
    begin
      Print('Kunde auswählen:   ');
      Self.FSelectCustomer.SetPosition;
      Self.FSelectCustomer.Draw;
    end;
    4:
    begin
      Print('Woche auswählen:   ');
      Self.FSelectWeek.SetPosition;
      Self.FSelectWeek.Draw;
    end;
    5:
    begin
      Print('Rechnungsnummer:   ');
      Self.FInvoiceNumber.SetPosition;
      Self.FInvoiceNumber.Draw;
    end;
  end;
end;

procedure TReservationMenu.Show;
begin
  while true do
  begin
    Self.Draw;
    
    if Self.FSelectView.Activate = SEscape then
      break;
    Self.Draw;//TInputSelect überschreibt möglicherweise den Bildschirm
    
    case Self.FSelectView.Value of
      0:
        ShowReservationsComplete;
      
      1:
        ReservationCreate;
      
      2:
      begin
        case Self.FSelectCamper.Activate of
          SEscape:
            break;
          SReturn:
            ShowReservationsByCamper(Self.FSelectCamper.Value);
        end;
      end;
      
      3:
      begin
        case Self.FSelectCustomer.Activate of
          SEscape:
            break;
          SReturn:
            ShowReservationsByCustomer(Self.FSelectCustomer.Value);
        end;
      end;
      
      4:
      begin
        case Self.FSelectWeek.Activate of
          SEscape:
            break;
          SReturn:
            ShowReservationsByWeek(Trunc(Self.FSelectWeek.Value));
        end;
      end;

      5:
      begin
        case Self.FInvoiceNumber.Activate of
          SEscape:
            break;
          SReturn:
          begin
            if not InvoiceViewByNumber(Trunc(Self.FInvoiceNumber.Value)) then
            begin
              PrintHead('Buchungen');
              PrintHelp(KeysBack);
              PrintLn;
              PrintLn('Es wurde keine Rechnung mit dieser Nummer gefunden.');
              
              while true do
              begin
                case ReadKey of
                  #0:
                    ReadKey;//ScanCode lesen und verwerfen
                  
                  #13, #27:
                    break;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

function CheckPreconditions: Boolean;
begin
  if Length(Data.Campers) = 0 then
  begin
    PrintHead('Buchungen');
    PrintHelp(KeysBack);
    PrintLn;
    PrintLn('Es sind noch keine Camper eingetragen.');
    PrintLn;
    PrintLn('Um eine Buchung anzulegen,');
    PrintLn('müssen Sie zunächst einen Camper definieren.');
    
    while true do
    begin
      case ReadKey of
        #0:
          ReadKey;//ScanCode lesen und verwerfen
        
        #13, #27:
          break;
      end;
    end;
    Result := false;
  end
  else
  begin
    if Length(Data.Customers) = 0 then
    begin
      PrintHead('Buchungen');
      PrintHelp(KeysBack);
      PrintLn;
      PrintLn('Es sind noch keine Kunden eingetragen.');
      PrintLn;
      PrintLn('Um eine Buchung anzulegen,');
      PrintLn('müssen Sie zunächst einen Kunden eintragen.');
      
      while true do
      begin
        case ReadKey of
          #0:
            ReadKey;//ScanCode lesen und verwerfen
          
          #13, #27:
            break;
        end;
      end;
      Result := false;
    end
    else
      Result := true;
  end;
end;

procedure ShowReservationMenu;
var
  Menu: TReservationMenu;
  Redraw: Boolean;
begin
  if CheckPreconditions then
  begin
    repeat
      Redraw := false;
      try
        Menu := TReservationMenu.Create;
        Menu.Show;
      except
        on TNoEntriesException do
        begin
          PrintHead('Buchungen');
          PrintHelp(KeysBack);
          PrintLn;
          PrintLn('Es wurden keine Buchungen gefunden, die in dieser Ansicht');
          PrintLn('angezeigt werden können.');
          PrintLn;
          PrintLn('Wählen Sie eine andere Ansicht,');
          PrintLn('oder erstellen Sie eine neue Buchung.');
          
          while true do
          begin
            case ReadKey of
              #0:
                ReadKey;//ScanCode lesen und verwerfen
              
              #13, #27:
                break;
            end;
          end;
          Redraw := true;
        end;
      end;
    until Redraw = false;
  end;
end;

initialization

SetLength(SelectViewModel, 6);
SelectViewModel[0] := TSimpleListRow.Create('Gesamtliste');
SelectViewModel[1] := TSimpleListRow.Create('Neue Buchung');
SelectViewModel[2] := TSimpleListRow.Create('Buchungen nach Camper');
SelectViewModel[3] := TSimpleListRow.Create('Buchungen nach Kunde');
SelectViewModel[4] := TSimpleListRow.Create('Buchungen nach Woche');
SelectViewModel[5] := TSimpleListRow.Create('Rechnung betrachten');

end.

