unit UInputText;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UInput;

type

TInputText = class
  private
    FActive:      Boolean;
    FCursorPos:   Integer;
    FEditValue:   AnsiString;
    FFieldLength: Integer;
    FMaxLength:   Integer;
    FPosition:    Integer;
    FPosX:        Integer;
    FPosY:        Integer;
    FSaveValue:   AnsiString;
    
    procedure Update;
  
  public
    property Value: AnsiString read FSaveValue;
    
    constructor Create(StrVal: AnsiString; MaxLength, FieldLength: Integer);
    function    Activate: TInputReturnKey;
    procedure   Draw;
    procedure   SetPosition;
end;

implementation

uses
  {$IFNDEF FPC}StrUtils,{$ENDIF}
  SysUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UStringUtils;

constructor TInputText.Create(StrVal: AnsiString; MaxLength, FieldLength: Integer);
begin
  if Length(StrVal) > MaxLength then
    StrVal := LeftStr(StrVal, 10);
  
  Self.SetPosition;
  
  Self.FActive      := false;
  Self.FCursorPos   := Length(StrVal);
  Self.FEditValue   := StrVal;
  Self.FFieldLength := FieldLength;
  Self.FMaxLength   := MaxLength;
  Self.FPosition    := Length(StrVal);
  Self.FSaveValue   := StrVal;
  Self.Update;
end;

function TInputText.Activate: TInputReturnKey;
var
  Action: TInputAction;
  KeyCode: Char;
begin
  Self.FActive := true;
  Self.FEditValue := Self.FSaveValue;
  Result := SEscape;
  Action := SCont;
  
  repeat
    Self.Draw;
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        KeyCode := ReadKey;
        case KeyCode of
          #71: // Numpad Pos1
            Self.FCursorPos := 0;
          
          #75: // Pfeil links
            dec(Self.FCursorPos);
          
          #77: // Pfeil rechts
            inc(Self.FCursorPos);
          
          #79: // Numpad Ende
            Self.FCursorPos := Length(Self.FEditValue);
          
          #83: // Entf
          if Self.FCursorPos < Length(Self.FEditValue) then
          begin
            Self.FEditValue := Copy(Self.FEditValue, 1, Self.FCursorPos)
                             + Copy(Self.FEditValue, Self.FCursorPos+2, Length(Self.FEditValue));
          end;
        end;
      end;
      
      #8: // Backspace
      begin
        if Self.FCursorPos > 0 then
        begin
          Self.FEditValue := Copy(Self.FEditValue, 1, Self.FCursorPos-1)
                           + Copy(Self.FEditValue, Self.FCursorPos+1, Length(Self.FEditValue));
          dec(Self.FCursorPos);
        end;
      end;
      
      #9: // Tab
      begin
        Action := SSave;
        Result := STab;
      end;
      
      #13: // Enter
      begin
        Action := SSave;
        Result := SReturn;
      end;
      
      #27: // Esc
      begin
        Action := SDiscard;
        Result := SEscape;
      end;
      
      #32 .. #126: // Druckbare Zeichen des ASCII-Zeichensatzes
      begin
        if Length(Self.FEditValue) <= Self.FMaxLength then
        begin
          Self.FEditValue := Copy(Self.FEditValue, 1, Self.FCursorPos)
                           + KeyCode
                           + Copy(Self.FEditValue, Self.FCursorPos+1, Length(Self.FEditValue));
          inc(Self.FCursorPos);
        end;
      end;
    end;
    
    Self.Update;
  until Action <> SCont;
  
  Self.FActive := false;
  
  if Action = SSave then
    Self.FSaveValue := Self.FEditValue
  else
    Self.FEditValue := Self.FSaveValue;
  
  Self.Draw;
end;

procedure TInputText.Update;
begin
  if Self.FCursorPos < 0 then
    Self.FCursorPos := 0;
  if Self.FCursorPos > Length(Self.FEditValue) then
    Self.FCursorPos := Length(Self.FEditValue);
  
  if Length(Self.FEditValue) <= Self.FFieldLength then
    Self.FPosition := 0
  else
  begin
    Self.FPosition := Self.FCursorPos - (Self.FFieldLength div 2);
    
    if Self.FPosition > Length(Self.FEditValue)-Self.FFieldLength then
      Self.FPosition := Length(Self.FEditValue)-Self.FFieldLength;
    if Self.FPosition < 0 then
      Self.FPosition := 0;
  end;
end;

procedure TInputText.Draw();
var
  VisibleStr: AnsiString;
  X,Y: Integer;
begin
  X := WhereX;
  Y := WhereY;
  Cursor(Self.FPosX, Self.FPosY);
  
  VisibleStr := Copy(Self.FEditValue, Self.FPosition+1, Self.FFieldLength);
  if not Self.FActive then
  begin
    Print(' %-'+IntToStr(Self.FFieldLength)+'s ', [VisibleStr]);
    Cursor(X, Y);
  end
  else
  begin
    TextBackground(White);
    TextColor(Red);
    if Self.FPosition = 0 then
      Print('[')
    else
      Print('←');
    
    TextColor(Black);
    Print(VisibleStr);
    
    TextColor(Red);
    print(RepeatChar('·', Self.FFieldLength - Length(VisibleStr)));
    
    if Self.FPosition + Length(VisibleStr) < Length(Self.FEditValue) then
      Print('→')
    else
      Print(']');
    
    NormVideo;
    Cursor(Self.FPosX+Self.FCursorPos-Self.FPosition+1, Self.FPosY);
  end;
end;

procedure TInputText.SetPosition;
begin
  Self.FPosX := WhereX;
  Self.FPosY := WhereY;
end;

end.

