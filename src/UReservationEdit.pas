unit UReservationEdit;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UInput,
  UInputButton,
  UInputNumber,
  UInputSelect,
  UReservation;

type

TReservationEditor = class
  private
    FReservation: TReservation;
    
    FCamper:      TInputSelect;
    FCustomer:    TInputSelect;
    FStartWeek:   TInputNumber;
    FEndWeek:     TInputNumber;
    
    FBtnCancel:   TInputButton;
    FBtnSave:     TInputButton;
  
  public
    constructor Create(Reservation: TReservation);overload;
    constructor Create(Camper, Customer, StartWeek, EndWeek: Integer);overload;
    procedure   Draw;
    function    Save: Boolean;
    function    Show: TInputAction;
end;

procedure ReservationCreate;overload;
procedure ReservationCreate(Camper, Customer, StartWeek, EndWeek: Integer);overload;
procedure ReservationEdit(Index: Integer);

implementation

uses
  UCamper,
  UCustomer,
  UData,
  UListView,
  UReserved,
  UStringUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF};

var
  Keys: array[0..1] of TKey = 
  (
    ('Tab', 'Zum nächsten Feld'),
    ('Esc', 'Zurück')
  );
  ColumnsCamper: array[0..4] of TColumn =
  (
    (Name: 'Stellplatz';  Content: SNumber; Source: 'number'; Length: 10; Padding: true; Decimals: 0),
    (Name: 'Modell';      Content: SString; Source: 'class';  Length: 10; Padding: true),
    (Name: 'Kennzeichen'; Content: SString; Source: 'plate';  Length: 11; Padding: true),
    (Name: 'Leistung';    Content: SNumber; Source: 'power';  Length:  8; Padding: true; Decimals: 0),
    (Name: 'Hubraum';     Content: SNumber; Source: 'engine'; Length:  7; Padding: true; Decimals: 1)
  );
  ColumnsCustomer: array[0..1] of TColumn =
  (
    (Name: 'Kunde';        Content: SString; Source: 'fullname'; Length: 30; Padding: true),
    (Name: 'Kundennummer'; Content: SNumber; Source: 'number';   Length: 12; Padding: true; Decimals: 0)
  );
  ErrorMsg : array of String;

constructor TReservationEditor.Create(Reservation: TReservation);
begin
  Self.FReservation := Reservation;
  
  Self.FBtnCancel := TInputButton.Create('Abbrechen');
  Self.FBtnSave   := TInputButton.Create('Speichern');
  
  if Reservation = nil then
  begin
    Self.FCamper    := TInputSelect.Create('Camper-Typ auswählen', ColumnsCamper,   Data.ListCampers,   0);
    Self.FCustomer  := TInputSelect.Create('Kunden auswählen',     ColumnsCustomer, Data.ListCustomers, 0);
    Self.FStartWeek := TInputNumber.Create(1, 1, MAX_WEEKS, true);
    Self.FEndWeek   := TInputNumber.Create(2, 2, MAX_WEEKS+1,   true);
  end
  else
  begin
    Self.FCamper    := TInputSelect.Create('Camper-Typ auswählen', ColumnsCamper,   Data.ListCampers,   Data.GetCamperByNumber(Reservation.Camper));
    Self.FCustomer  := TInputSelect.Create('Kunden auswählen',     ColumnsCustomer, Data.ListCustomers, Data.GetCamperByNumber(Reservation.Customer));
    Self.FStartWeek := TInputNumber.Create(Reservation.StartWeek, 1, MAX_WEEKS, true);
    Self.FEndWeek   := TInputNumber.Create(Reservation.EndWeek,   2, MAX_WEEKS+1,   true);
  end;
end;

constructor TReservationEditor.Create(Camper, Customer, StartWeek, EndWeek: Integer);
begin
  Self.Create(nil);
  Self.FCamper.Value    := Camper;
  Self.FCustomer.Value  := Customer;
  Self.FStartWeek.Value := StartWeek;
  Self.FEndWeek.Value   := EndWeek;
end;

procedure TReservationEditor.Draw;
var 
  I: Integer;
begin
  PrintHead('Buchung bearbeiten');
  PrintHelp(Keys);
  PrintLn;
  
  ReservedBarForCamperIndex(Self.FCamper.Value);
  PrintLn;
  
  Print('Camper:     ');
  Self.FCamper.SetPosition;
  Self.FCamper.Draw;
  PrintLn;
  
  Print('Kunde:      ');
  Self.FCustomer.SetPosition;
  Self.FCustomer.Draw;
  PrintLn;
  
  Print('Startwoche: ');
  Self.FStartWeek.SetPosition;
  Self.FStartWeek.Draw;
  PrintLn;
  
  Print('Endwoche:   ');
  Self.FEndWeek.SetPosition;
  Self.FEndWeek.Draw;
  PrintLn;
  
  PrintLn;
  Self.FBtnSave.SetPosition;
  Print('               ');
  Self.FBtnCancel.SetPosition;
  
  Self.FBtnSave.Draw;
  Self.FBtnCancel.Draw;
  
  // Draw error messages
  if (Length(ErrorMsg) > 0) then
  begin
    PrintLn;
    PrintLn;
    TextColor(Red);
    PrintLn('Beim Speichern sind folgende Fehler aufgetreten:');
    for I := 0 to High(ErrorMsg) do
    begin
      PrintLn('> ' + ErrorMsg[I]);
    end;
    NormVideo;
  end;
end;

function TReservationEditor.Show: TInputAction;
begin
  Result := SDiscard;
  // At the beginning, we have no errors.
  SetLength(ErrorMsg, 0);
  
  while true do
  begin
    Self.Draw;
    
    if Self.FCamper.Activate = SEscape then
      break;
    Self.Draw;
    
    if Self.FCustomer.Activate = SEscape then
      break;
    Self.Draw;
    
    if Self.FStartWeek.Activate = SEscape then
      break;
    
    if Self.FEndWeek.Activate = SEscape then
      break;
    
    case Self.FBtnSave.Activate of
      SSpace, SReturn:
      begin
        if Self.Save then
        begin
          Result := SSave;
          break;
        end;
        Self.Draw;
      end;
      
      SEscape:
        break;
    end;
    
    if Self.FBtnCancel.Activate <> STab then
      break;
  end;
end;

function TReservationEditor.Save: Boolean;
var
  Reservation: TReservation;
  I: Integer;
  Camper: TCamper;
  Customer: TCustomer;
begin
  //TODO Werte überprüfen
  Result := true;
  SetLength(ErrorMsg, 0);

  Camper := Data.Campers[Trunc(Self.FCamper.Value)];
  Customer := Data.Customers[Trunc(Self.FCustomer.Value)];
  
  if Trunc(Self.FEndWeek.Value) <= Trunc(Self.FStartWeek.Value) then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Endwoche muss größer als die Startwoche sein!';
    Result := false;
  end;
  
  // Lets check whether the selected camper is reserved in the selected time.
  for I := 0 to High(Data.Reservations) do
  begin
    if (Data.Reservations[I].Camper = Camper.Number) and 
      ( (Self.FReservation = nil) or (Self.FReservation <> Data.Reservations[I])) then
    begin
      {*
        StL = Start Local
        EnL = End Local
        StR = Start remote
        EnR = End remote
        
        Wenn StL <  EnR & EnL > StR => Fehler! ODER
        Wenn EnR <= StL & StR >= EnL => Fehler!
      *}
      if ((Trunc(Self.FStartWeek.Value) < Data.Reservations[I].EndWeek) and 
        (Trunc(Self.FEndWeek.Value) > Data.Reservations[I].StartWeek)) or
        ((Data.Reservations[I].EndWeek <= Trunc(Self.FStartWeek.Value)) and 
        (Data.Reservations[I].StartWeek >= Trunc(Self.FEndWeek.Value))) then
      begin
        SetLength(ErrorMsg, Length(ErrorMsg) + 1);
        ErrorMsg[High(ErrorMsg)] := 'Die Reservierung des Campers überschneidet sich mit einer bestehenden Reservierung!';
        Result := false;
        break;
      end;
    end;
  end;
  
  if (Self.FReservation = nil) and Result then
  begin
    Reservation := TReservation.Create(Customer.Number,
                                       Camper.Number,
                                       Trunc(Self.FStartWeek.Value),
                                       Trunc(Self.FEndWeek.Value));
    
    Data.AddReservation(Reservation);
  end
  else if Result then
  begin
    Self.FReservation.Customer  := Customer.Number;
    Self.FReservation.Camper    := Camper.Number;
    Self.FReservation.StartWeek := Trunc(Self.FStartWeek.Value);
    Self.FReservation.EndWeek   := Trunc(Self.FEndWeek.Value);
  end;
end;

procedure ReservationCreate;
var
  Editor: TReservationEditor;
begin
  Editor := TReservationEditor.Create(nil);
  Editor.Show;
end;

procedure ReservationCreate(Camper, Customer, StartWeek, EndWeek: Integer);
var
  Editor: TReservationEditor;
begin
  Editor := TReservationEditor.Create(Camper, Customer, StartWeek, EndWeek);
  Editor.Show;
end;

procedure ReservationEdit(Index: Integer);
var
  Editor: TReservationEditor;
begin
  Editor := TReservationEditor.Create(Data.Reservations[Index]);
  Editor.Show;
end;

end.

