unit UCamperClass;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UListModel;

type

TCamperVariant = (Standard, Luxury);

TCamperClass = class(TInterfacedObject, IListRow)
  public
    Variant: TCamperVariant;
    Price, Engine: Single;
    Size, Power: Integer;
    Name: String;
    
    constructor Create(AName: String; ASize: Integer; AVariant: TCamperVariant; APrice, AEngine: Single; APower: Integer);
    constructor Deserialize(Data: AnsiString);
    function    GetBoolean(Column: ShortString): Boolean;
    function    GetNumber (Column: ShortString): Single;
    function    GetString (Column: ShortString): String;
    function    Serialize: AnsiString;
end;

TCamperClasses = array of TCamperClass;

function GetVariantName(CamperVariant: TCamperVariant): String;

implementation

uses
  Classes,
  SysUtils;

constructor TCamperClass.Create(AName: String; ASize: Integer; AVariant: TCamperVariant; APrice, AEngine: Single; APower: Integer);
begin
  Self.Name    := AName;
  Self.Size    := ASize;
  Self.Variant := AVariant;
  Self.Price   := APrice;
  Self.Engine  := AEngine;
  Self.Power   := APower;
end;

constructor TCamperClass.Deserialize(Data: AnsiString);
var
  Container: TStringList;
begin
  Container := TStringList.Create;
  Container.DelimitedText := Data;
  
  Self.Size    := StrToInt(Container.Values['size']);
  Self.Variant := TCamperVariant(StrToInt(Container.Values['variant']));
  Self.Price   := StrToFloat(Container.Values['price']);
  Self.Engine  := StrToFloat(Container.Values['engine']);
  Self.Power   := StrToInt(Container.Values['power']);
  Self.Name    := Container.Values['name'];
  
  FreeAndNil(Container);
end;

function TCamperClass.Serialize(): AnsiString;
var
  Container: TStringList;
begin
  Container := TStringList.Create();
  
  Container.Values['size']    := IntToStr(Self.Size);
  Container.Values['variant'] := IntToStr(Integer(Self.Variant));
  Container.Values['price']   := FloatToStr(Self.Price);
  Container.Values['engine']  := FloatToStr(Self.Engine);
  Container.Values['power']   := IntToStr(Self.Power);
  Container.Values['name']    := Self.Name;
  
  Result := Container.DelimitedText;
  
  FreeAndNil(Container);
end;

function TCamperClass.GetString(Column: ShortString): String;
begin
  Result := '';
  
  if Column = 'name' then
    Result := Self.Name;
  if Column = 'variant' then
    Result := GetVariantName(Self.Variant);
end;

function TCamperClass.GetNumber(Column: ShortString): Single;
begin
  Result := 0;
  if Column = 'price' then
    Result := Self.Price;
  if Column = 'power' then
    Result := Self.Power;
  if Column = 'engine' then
    Result := Self.Engine;
  if Column = 'size' then
    Result := Self.Size;
end;

function TCamperClass.GetBoolean(Column: ShortString): Boolean;
begin
  Result := false;
end;

function GetVariantName(CamperVariant: TCamperVariant): String;
begin
  case CamperVariant of
    Luxury: Result := 'Luxus';
    else    Result := 'Standard';
  end;
end;

end.
