unit UInputNumber;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UInput;

type

TInputNumber = class
  private
    FActive:    Boolean;
    FInteger:   Boolean;
    FEditValue: Single;
    FMin:       Single;
    FMax:       Single;
    FPosX:      Integer;
    FPosY:      Integer;
    FSaveValue: Single;
    FStepSmall: Single;
    FStepLarge: Single;
    FString:    String;
    
    procedure   Update;
    procedure   WriteValue(AValue: Single);
  
  public
    property    Value:     Single read FSaveValue write WriteValue;
    property    StepSmall: Single read FStepSmall write FStepSmall;
    property    StepLarge: Single read FStepLarge write FStepLarge;
    
    constructor Create(NumVal, MinVal, MaxVal: Single; IsInteger: Boolean);
    function    Activate: TInputReturnKey;
    procedure   Draw;
    procedure   SetPosition;
    function    StringMode: Boolean; overload;
    procedure   StringMode(Enter: Boolean); overload;
end;

implementation

uses
  SysUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UStringUtils;

constructor TInputNumber.Create(NumVal, MinVal, MaxVal: Single; IsInteger: Boolean);
begin
  Self.SetPosition;
  
  Self.FInteger   := IsInteger;
  Self.FEditValue := NumVal;
  Self.FSaveValue := NumVal;
  Self.FMin       := MinVal;
  Self.FMax       := MaxVal;
  Self.FString    := #0;
  Self.FStepSmall := 1;
  Self.FStepLarge := 10;
  
  Self.Update;
end;

function TInputNumber.Activate: TInputReturnKey;
var
  Action: TInputAction;
  KeyCode: Char;
begin
  Self.FActive := true;
  Self.FEditValue := Self.FSaveValue;
  Result := SEscape;
  Action := SCont;
  
  repeat
    Self.Draw;
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        KeyCode := ReadKey;
        case KeyCode of
          #72: // Pfeil hoch
          begin
            Self.StringMode(false);
            Self.FEditValue := Self.FEditValue + Self.FStepSmall;
            
            Update;
          end;
          
          #73: // Bild auf
          begin
            Self.StringMode(false);
            Self.FEditValue := Self.FEditValue + Self.FStepLarge;
            
            Update;
          end;
          
          #80: // Pfeil runter
          begin
            Self.StringMode(false);
            Self.FEditValue := Self.FEditValue - Self.FStepSmall;
            
            Update;
          end;
          
          #81: // Bild ab
          begin
            Self.StringMode(false);
            Self.FEditValue := Self.FEditValue - FStepLarge;
            
            Update;
          end;
        end;
      end;
      
      #8: // Backspace
      begin
        Self.StringMode(true);
        
        if Length(Self.FString) > 0 then
        begin
          Self.FString := Copy(Self.FString, 1, Length(Self.FString) - 1);
        end;
      end;
      
      #9: // Tab
      begin
        Action := SSave;
        Result := STab;
      end;
      
      #13: // Enter
      begin
        Action := SSave;
        Result := SReturn;
      end;
      
      #27: // Esc
      begin
        Action := SDiscard;
        Result := SEscape;
      end;
      
      #44, #46:
      begin
        Self.StringMode(true);
        
        if Pos('.', Self.FString) = 0 then
          Self.FString := Self.FString + '.';
        if not Self.FInteger then
        begin
          Self.StringMode(true);
          
          if Pos('.', Self.FString) = 0 then
            Self.FString := Self.FString + '.';
        end;
      end;
      
      #48 .. #57:
      begin
        Self.StringMode(true);
        Self.FString := Self.FString + KeyCode;
      end;
    end;
    
    Self.Update;
  until Action <> SCont;
  
  Self.StringMode(false);
  
  Self.FActive := false;
  
  if Action = SSave then
    Self.FSaveValue := Self.FEditValue
  else
    Self.FEditValue := Self.FSaveValue;
  
  Self.Draw;
end;

procedure TInputNumber.Update;
begin
  if Self.FEditValue > Self.FMax then
    Self.FEditValue := Self.FMax;
  
  if Self.FEditValue < Self.FMin then
    Self.FEditValue := Self.FMin;
  
  if Self.FInteger then
  begin
    Self.FEditValue := Int(Self.FEditValue);
  end;
end;

procedure TInputNumber.WriteValue(AValue: Single);
begin
  Self.FEditValue := AValue;
  Self.Update;
  Self.FSaveValue := Self.FEditValue;
end;

procedure TInputNumber.Draw();
var
  X,Y: Integer;
begin
  X := WhereX;
  Y := WhereY;
  
  Cursor(Self.FPosX, Self.FPosY);
  
  if not Self.FActive then
  begin
    if Self.FInteger then
      Print('%10s', [FloatToStrF(Self.FEditValue, ffFixed, 10, 0)])
    else
      Print('%10s', [FloatToStrF(Self.FEditValue, ffFixed, 10, 2)]);
    
    Print(' '); // Spinner überschreiben
    Cursor(X, Y);
  end
  else
  begin
    TextBackground(White);
    TextColor(Black);
    if Self.StringMode then
      Print('%10s', [Self.FString])
    else
      if Self.FInteger then
        Print('%10s', [FloatToStrF(Self.FEditValue, ffFixed, 10, 0)])
      else
        Print('%10s', [FloatToStrF(Self.FEditValue, ffFixed, 10, 2)]);
    
    TextColor(Red);
    
    if Self.FEditValue = FMin then
      Print('↑')
    else if Self.FEditValue = FMax then
      Print('↓')
    else
      Print('↕');
    
    NormVideo;
    Cursor(Self.FPosX+10, Self.FPosY);
  end;
end;

procedure TInputNumber.SetPosition;
begin
  Self.FPosX := WhereX;
  Self.FPosY := WhereY;
end;

function TInputNumber.StringMode: Boolean;
begin
  Result := Self.FString <> #0;
end;

procedure TInputNumber.StringMode(Enter: Boolean);
begin
  if Enter then
  begin
    if not Self.StringMode then
    begin
      if Self.FInteger then
        Self.FString := FloatToStrF(Self.FEditValue, ffFixed, 10, 0)
      else
        Self.FString := FloatToStrF(Self.FEditValue, ffFixed, 10, 2);
    end;
  end
  else
  begin
    if Self.StringMode then
    begin
      if Self.FString = '' then
        Self.FEditValue := 0
      else
        Self.FEditValue := StrToFloat(Self.FString);
      Self.FString := #0;
    end;
    Self.Update;
  end;
end;

end.

