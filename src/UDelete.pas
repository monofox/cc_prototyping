unit UDelete;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  SysUtils,
  UPaging;

type

EStillReferenced = class(Exception)
end;

function DeleteCamper(Paging: TPaging): Boolean;
function DeleteCamperClass(Paging: TPaging): Boolean;
function DeleteCustomer(Paging: TPaging): Boolean;
function DeleteReservation(Index: Integer): Boolean;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UData,
  UStringUtils;

var
  Keys: array[0..1] of TKey =
  (
    ('j', 'Ja (Löschen)'),
    ('n,Esc', 'Nein (Abbrechen)')
  );

procedure RemoveCamper(Index: Integer);
var
  I: Integer;
  Found: Boolean;
begin
  // In reservation? 
  Found := false;
  for I := 0 to High(Data.Reservations) do
  begin
    if Data.Reservations[I].Camper = Data.Campers[Index].Number then
    begin
      PrintHead('Camper löschen - Fehler');
      PrintLn;
      PrintLn('Der Camper #'+IntToStr(Data.Campers[Index].Number)+' kann nicht gelöscht werden!');
      PrintLn('Bitte löschen Sie zunächst alle Buchungen, die auf diesen Camper notiert sind.');
      ReadKey(); 

      Found := true;
      break;
    end;
  end;

  if not Found then
  begin
    Move(Data.Campers[Index + 1], Data.Campers[Index], SizeOf(Data.Campers[0]) * (Length(Data.Campers) - Index - 1));
    SetLength(Data.Campers, Length(Data.Campers) - 1);

    // Update references Camper <-> CamperClass
    // We will update all campers where the camper class is greather than the camper class we will remove
    Data.UpdateReferenceCamperClass(Index);
  end;

end;

procedure RemoveCamperClass(Index: Integer);
var
  I: Integer;
  Found: Boolean;
begin
  Found := false;
  for I := 0 to High(Data.Campers) do
  begin
    if Data.Campers[I].CamperClass = Index then
    begin
      PrintHead('Camper-Typ löschen - Fehler');
      PrintLn;
      PrintLn('Der Camper-Typ "'+Data.CamperClasses[Index].Name+'" kann nicht gelöscht werden!');
      PrintLn('Sie müssen erst alle Camper diesen Typs löschen.');
      ReadKey(); 

      Found := true;
      break;
    end;
  end;
  
  if not Found then
  begin
    Move(Data.CamperClasses[Index + 1], Data.CamperClasses[Index], SizeOf(Data.CamperClasses[0]) * (Length(Data.CamperClasses) - Index - 1));
    SetLength(Data.CamperClasses, Length(Data.CamperClasses) - 1);
  end;
end;

procedure RemoveCustomer(Index: Integer);
var
  I: Integer;
  Found: Boolean;
begin
  Found := false;
  for I := 0 to High(Data.Reservations) do
  begin
    if Data.Reservations[I].Customer = Data.Customers[Index].Number then
    begin
      PrintHead('Kunde löschen - Fehler');
      PrintLn;
      PrintLn('Der Kunde #'+IntToStr(Data.Customers[Index].Number)+' kann nicht gelöscht werden!');
      PrintLn('Bitte löschen Sie zunächst die zugehörigen Buchungen.');
      ReadKey(); 

      Found := true;
      break;
    end;
  end;

  if not Found then 
  begin
    Move(Data.Customers[Index + 1], Data.Customers[Index], SizeOf(Data.Customers[0]) * (Length(Data.Customers) - Index - 1));
    SetLength(Data.Customers, Length(Data.Customers) - 1);
  end;
end;

procedure RemoveReservation(Index: Integer);
begin
  // There does not exist any references!
  Move(Data.Reservations[Index + 1], Data.Reservations[Index], SizeOf(Data.Reservations[0]) * (Length(Data.Reservations) - Index - 1));
  SetLength(Data.Reservations, Length(Data.Reservations) - 1);
end;

function DeleteCamper(Paging: TPaging): Boolean;
var
  Key: Char;
  Count, I: Integer;
begin
  Result := false;// Warnung unterdrücken
  
  while true do
  begin
    PrintHead('Camper löschen');
    PrintHelp(Keys);
    PrintLn;
    
    Count := Paging.CountSelected;
    if Count < 2 then
      Print('Soll dieser Eintrag wirklich gelöscht werden?')
    else
      Print('Sollen die markierten %d Einträge wirklich gelöscht werden?', [Count]);
    
    Key := ReadKey();
    
    if Key = #0 then
      ReadKey(); // ScanCode lesen und verwerfen
    
    if (LowerCase(Key) = 'n') or (Key = #27) then
    begin
      Result := false;
      break;
    end;
    
    if LowerCase(Key) = 'j' then
    begin
      if Count = 0 then
      begin
        RemoveCamper(Paging.SelectedIndex);
      end
      else
      begin
        for I := Paging.Length - 1 downto 0 do
        begin
          if not Paging.IsSelected(I) then
            continue;
          
          RemoveCamper(I);
          Paging.Unselect(I);
        end;
      end;
      
      Paging.Length := Length(Data.Campers);
      Result := true;
      break;
    end;
  end;
end;

function DeleteCamperClass(Paging: TPaging): Boolean;
var
  Key: Char;
  Count, I: Integer;
begin
  Result := false;// Warnung unterdrücken
  
  while true do
  begin
    PrintHead('Camper-Typen löschen');
    PrintHelp(Keys);
    PrintLn;
    
    Count := Paging.CountSelected;
    if Count < 2 then
      Print('Soll dieser Eintrag wirklich gelöscht werden?')
    else
      Print('Sollen die markierten %d Einträge wirklich gelöscht werden?', [Count]);
    
    Key := ReadKey();
    
    if Key = #0 then
      ReadKey(); // ScanCode lesen und verwerfen
    
    if (LowerCase(Key) = 'n') or (Key = #27) then
    begin
      Result := false;
      break;
    end;
    
    if LowerCase(Key) = 'j' then
    begin
      if Count = 0 then
      begin
        RemoveCamperClass(Paging.SelectedIndex);
      end
      else
      begin
        for I := Paging.Length - 1 downto 0 do
        begin
          if not Paging.IsSelected(I) then
            continue;
          
          RemoveCamperClass(I);
          Paging.Unselect(I);
        end;
      end;
      
      Paging.Length := Length(Data.CamperClasses);
      Result := true;
      break;
    end;
  end;
end;

function DeleteCustomer(Paging: TPaging): Boolean;
var
  Key: Char;
  Count, I: Integer;
begin
  Result := false;// Warnung unterdrücken
  
  while true do
  begin
    PrintHead('Kunden löschen');
    PrintHelp(Keys);
    PrintLn;
    
    Count := Paging.CountSelected;
    if Count < 2 then
      Print('Soll dieser Eintrag wirklich gelöscht werden?')
    else
      Print('Sollen die markierten %d Einträge wirklich gelöscht werden?', [Count]);
    
    Key := ReadKey();
    
    if Key = #0 then
      ReadKey(); // ScanCode lesen und verwerfen
    
    if LowerCase(Key) = 'n' then
    begin
      Result := false;
      break;
    end;
    
    if LowerCase(Key) = 'j' then
    begin
      if Count = 0 then
      begin
        RemoveCustomer(Paging.SelectedIndex);
      end
      else
      begin
        for I := Paging.Length - 1 downto 0 do
        begin
          if not Paging.IsSelected(I) then
            continue;
          
          RemoveCustomer(I);
          Paging.Unselect(I);
        end;
      end;
      
      Paging.Length := Length(Data.Customers);
      Result := true;
      break;
    end;
  end;
end;

function DeleteReservation(Index: Integer): Boolean;
var
  Key: Char;
begin
  Result := false;// Warnung unterdrücken
  
  while true do
  begin
    PrintHead('Reservierung löschen');
    PrintHelp(Keys);
    PrintLn;
    Print('Soll dieser Eintrag wirklich gelöscht werden?');
    
    Key := ReadKey();
    
    if Key = #0 then
      ReadKey(); // ScanCode lesen und verwerfen
    
    if (LowerCase(Key) = 'n') or (Key = #27) then
    begin
      Result := false;
      break;
    end;
    
    if LowerCase(Key) = 'j' then
    begin
      RemoveReservation(Index);
      Result := true;
      break;
    end;
  end;
end;

end.

