unit UStringUtils;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

type


TKey = array[0..1] of ShortString;

procedure Cursor(X, Y: Integer);
procedure Print(Input: String);overload;
procedure Print(const Fmt: String; const Args: array of const);overload;
procedure PrintLn;overload;
procedure PrintLn(Input: String);overload;
procedure PrintLn(const Fmt: String; const Args: array of const);overload;
procedure PrintHead(Title: String);
procedure PrintHelp(Keys: array of TKey);
function  RepeatChar(Input: String; Count: Integer): String;
function  SpecialChars(Input: AnsiString): AnsiString;

implementation

uses
  SysUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF};

const
{$IFDEF MSWINDOWS}
  // Format: UTF-8, DOS
  Chars: array[0..18] of array[0..1] of ShortString =
  (
    ('↕', #18),
    ('↑', #24),
    ('↓', #25),
    ('→', #26),
    ('←', #27),
    ('▼', #31),
    ('☐', #32), // Space
    ('$', #36),
    ('☑', #42), // Alternative (*)
    ('☒', #42), // Alternative (*)
    ('ä', #132),
    ('ö', #148),
    ('ü', #129),
    ('Ä', #142),
    ('Ö', #153),
    ('Ü', #154),
    ('…', #175), // Alternative (»)
    ('ß', #225),
    ('·', #250)
  );
{$ENDIF}
  
  MAXWIDTH = 80;

procedure Print(Input: String);
begin
  Write(SpecialChars(Input));
end;

procedure Cursor(X, Y: Integer);
begin
  GotoXY(1, Y);
  GotoXY(X, Y);
end;

procedure Print(const Fmt: String; const Args: array of const);
begin
  Write(SpecialChars(Format(Fmt, Args)));
end;

procedure PrintLn;
begin
  WriteLn;
end;

procedure PrintLn(Input: String);
begin
  WriteLn(SpecialChars(Input));
end;

procedure PrintLn(const Fmt: String; const Args: array of const);
begin
  WriteLn(SpecialChars(Format(Fmt, Args)));
end;

function SpecialChars(Input: AnsiString): AnsiString;
{$IFDEF MSWINDOWS}
var
  I: Integer;
{$ENDIF}
begin
  Result := Input;
  {$IFDEF MSWINDOWS}
    for i := 0 to Length(Chars) - 1 do
      Result := StringReplace(Result, Chars[I][0], Chars[I][1], [rfReplaceAll]);
  {$ENDIF}
end;

procedure ClearLn;
var
  Y: Integer;
begin
  Y := WhereY;
  GotoXY(1, Y);
  Print(StringOfChar(' ', MAXWIDTH));
  GotoXY(1, Y);
end;

procedure PrintHead(Title: String);
var
  SpaceBefore: Integer;
begin
  Title := SpecialChars(Title);
  SpaceBefore := (MAXWIDTH DIV 2) - (Length(Title) DIV 2) - 2;
  
  ClrScr;
  TextBackground(Red);
  TextColor(White);
  ClearLn;
  PrintLn(' ==============================================================================');
  ClearLn;
  PrintLn(' =                              Canadian Camper                               =');
  ClearLn;
  Print(' =' + StringOfChar(' ', SpaceBefore) + Title);
  Cursor(MAXWIDTH-1, WhereY);
  PrintLn('=');
  ClearLn;
  PrintLn(' ==============================================================================');
  NormVideo;
end;

procedure PrintHelp(Keys: array of TKey);
var
  I: Integer;
begin
  TextBackground(White);
  ClearLn;
  Print(' '); // Einrückung wie Titel
  for I := 0 to High(Keys) do
  begin
    TextColor(Blue);
    Print(Keys[I][0]+': ');
    TextColor(LightBlue);
    Print(Keys[I][1]+'   ');
  end;
  PrintLn;
  NormVideo;
end;

function  RepeatChar(Input: String; Count: Integer): String;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Count do
    Result := Result + Input;
end;

end.
