unit UCamperEdit;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UCamper,
  UInput,
  UInputButton,
  UInputNumber,
  UInputText,
  UInputSelect,
  UPaging;

type

TCamperEditor = class
  private
    FCamper:      TCamper;
    FCamperClass: TInputSelect;
    FNumber:      TInputNumber;
    FPlate:       TInputText;
    FBtnCancel:   TInputButton;
    FBtnCont:     TInputButton;
    FBtnSave:     TInputButton;
    FMultiple:    Boolean;
  
  public
    constructor Create(Camper: TCamper; Multiple: Boolean);
    procedure   Draw;
    function    Save: Boolean;
    function    Show: TInputAction;
end;

procedure CamperCreate;
procedure CamperEdit(Paging: TPaging);

implementation

uses
  UData,
  UListView,
  USort,
  UStringUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF};

var
  Keys: array[0..1] of TKey = 
  (
    ('Tab', 'Zum nächsten Feld'),
    ('Esc', 'Zurück')
  );
  Columns: array[0..0] of TColumn = 
  (
    (Name: 'Typ'; Content: SString; Source: 'name'; Length: 10; Padding: true)
  );
  ErrorMsg: array of String;

constructor TCamperEditor.Create(Camper: TCamper; Multiple: Boolean);
begin
  Self.FCamper := Camper;
  
  Self.FMultiple := Multiple;
  
  Self.FBtnCancel := TInputButton.Create('Abbrechen');
  Self.FBtnSave   := TInputButton.Create('Speichern');
  
  if Camper = nil then
  begin
    Self.FBtnCont := TInputButton.Create('Speichern und weiteren Camper eintragen');
    
    Self.FCamperClass := TInputSelect.Create('Camper-Typ auswählen', Columns, Data.ListCamperClasses, 0);
    Self.FNumber      := TInputNumber.Create(1, 1, 100, true);
    Self.FPlate       := TInputText.Create('', 100, 20);
  end
  else
  begin
    Self.FBtnCont := TInputButton.Create('Speichern und nächsten Camper bearbeiten');
    
    Self.FCamperClass := TInputSelect.Create('Camper-Typ auswählen', Columns, Data.ListCamperClasses, Camper.CamperClass);
    Self.FNumber      := TInputNumber.Create(Camper.Number, 1, 100, true);
    Self.FPlate       := TInputText.Create(Camper.Plate, 100, 20);
  end;
end;

procedure TCamperEditor.Draw;
var 
  I: Integer;
begin
  PrintHead('Camper bearbeiten');
  PrintHelp(Keys);
  PrintLn;
  
  Print('Camper-Typ:  ');
  Self.FCamperClass.SetPosition;
  Self.FCamperClass.Draw;
  PrintLn;
  
  Print('Stellplatz:  ');
  Self.FNumber.SetPosition;
  Self.FNumber.Draw;
  PrintLn;
  
  Print('Kennzeichen: ');
  Self.FPlate.SetPosition;
  Self.FPlate.Draw;
  PrintLn;
  
  if Self.FMultiple then
  begin
    PrintLn;
    Self.FBtnCont.SetPosition;
    Self.FBtnCont.Draw;
  end;
  
  PrintLn;
  Self.FBtnSave.SetPosition;
  Print('               ');
  Self.FBtnCancel.SetPosition;
  
  Self.FBtnSave.Draw;
  Self.FBtnCancel.Draw;

  // Draw error messages
  if (Length(ErrorMsg) > 0) then
  begin
    PrintLn;
    PrintLn;
    TextColor(Red);
    PrintLn('Beim Speichern sind folgende Fehler aufgetreten:');
    for I := 0 to High(ErrorMsg) do
    begin
      PrintLn('> ' + ErrorMsg[I]);
    end;
    NormVideo;
  end;
end;

function TCamperEditor.Show: TInputAction;
begin
  Result := SDiscard;
  // At the beginning, we have no errors.
  SetLength(ErrorMsg, 0);

  while true do
  begin
    Self.Draw;
    
    if Self.FCamperClass.Activate = SEscape then
      break;
    
    Self.Draw; // CamperClass überschreibt möglicherweise den Bildschirm
    
    if Self.FNumber.Activate = SEscape then
      break;
    
    if Self.FPlate.Activate = SEscape then
      break;
    
    if Self.FMultiple then
    begin
      case Self.FBtnCont.Activate of
        SSpace, SReturn:
        begin
          if Self.Save then
          begin
            Result := SCont;
            break
          end;
          Self.Draw;
        end;
        
        SEscape:
          break;
      end;
    end;
    
    case Self.FBtnSave.Activate of
      SSpace, SReturn:
      begin
        if Self.Save then
        begin
          Result := SSave;
          break;
        end;
        Self.Draw;
      end;
      
      SEscape:
        break;
    end;
    
    if Self.FBtnCancel.Activate <> STab then
      break;
  end;
end;

function TCamperEditor.Save: Boolean;
var
  Camper: TCamper;
begin
  Result := true;
  SetLength(ErrorMsg, 0);

  // Set message
  if Length(Self.FPlate.Value) <= 0 then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Die Eingabe des Kennzeichens ist Pflicht!';
    Result := false;
  end
  else
  begin
    // Check whether the name does already exist!
    // Only check, if its not changed
    if ((Self.FCamper <> nil) and (Self.FCamper.Plate <> Self.FPlate.Value)) or
       (Self.FCamper = nil) then
    begin
      if Data.GetCamperByPlate(Self.FPlate.Value) > -1 then
      begin
        SetLength(ErrorMsg, Length(ErrorMsg) + 1);
        ErrorMsg[High(ErrorMsg)] := 'Es existiert bereits ein Camper mit dem angegebenen Kennzeichen!';
        Result := false; 
      end;
    end;
  end;

  if Trunc(Self.FNumber.Value) <= 0 then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Die Eingabe des Stellplatzes ist Pflicht!';
    Result := false;
  end
  else
  begin
    // Check whether the name does already exist!
    // Only check, if its not changed
    if ((Self.FCamper <> nil) and (Self.FCamper.Number <> Self.FNumber.Value)) or
       (Self.FCamper = nil) then
    begin
      if Data.GetCamperByNumber(Trunc(Self.FNumber.Value)) > -1 then
      begin
        SetLength(ErrorMsg, Length(ErrorMsg) + 1);
        ErrorMsg[High(ErrorMsg)] := 'Es existiert bereits ein Camper mit dem angegebenen Stellplatz!';
        Result := false; 
      end;
    end;
  end;

  if (Self.FCamper = nil) and Result then
  begin
    Camper := TCamper.Create(Trunc(Self.FNumber.Value), Self.FCamperClass.Value, Self.FPlate.Value);
    Data.AddCamper(Camper);
  end
  else if Result then
  begin
    Camper := Self.FCamper;
    if Camper.Number <> Trunc(Self.FNumber.Value) then
      Data.UpdateReferenceCamper(Camper.Number, Trunc(Self.FCamperClass.Value));

    Camper.Number      := Trunc(Self.FNumber.Value);
    Camper.CamperClass := Self.FCamperClass.Value;
    Camper.Plate       := Self.FPlate.Value;
  end;
end;

procedure CamperCreate;
var
  Editor: TCamperEditor;
begin
  while true do
  begin
    Editor := TCamperEditor.Create(nil, true);
    if Editor.Show <> SCont then
      break;
  end;
  SortCampers;
end;

procedure CamperEdit(Paging: TPaging);
var
  Editor: TCamperEditor;
  Count, I: Integer;
begin
  Count := Paging.CountSelected;
  if Count = 0 then
  begin
    Editor := TCamperEditor.Create(Data.Campers[Paging.SelectedIndex], false);
    Editor.Show;
  end
  else
  begin
    for I := 0 to Paging.Length - 1 do
    begin
      if not Paging.IsSelected(I) then
        continue;
      
      Editor := TCamperEditor.Create(Data.Campers[I], true);
      Editor.Show;
    end;
  end;
  SortCampers;
end;

end.

