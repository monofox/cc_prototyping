unit UCamper;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UListModel;

type

TCamper = class(TInterfacedObject, IListRow)
  public
    Number, CamperClass: Integer;
    Plate: String;
    
    constructor Create(ANumber, ACamperClass: Integer; APlate: String);
    constructor Deserialize(Data: AnsiString);
    function    GetBoolean(Column: ShortString): Boolean;
    function    GetNumber (Column: ShortString): Single;
    function    GetString (Column: ShortString): String;
    function    Serialize: AnsiString;
end;

TCampers = array of TCamper;

implementation

uses
  Classes,
  SysUtils,
  UCamperClass,
  UData;

constructor TCamper.Create(ANumber, ACamperClass: Integer; APlate: String);
begin
  Self.Number      := ANumber;
  Self.CamperClass := ACamperClass;
  Self.Plate       := APlate;
end;

constructor TCamper.Deserialize(Data: AnsiString);
var
  Container: TStringList;
begin
  Container := TStringList.Create;
  Container.DelimitedText := Data;

  Self.Number      := StrToInt(Container.Values['number']);
  Self.CamperClass := StrToInt(Container.Values['camperclass']);
  Self.Plate       := Container.Values['plate'];

  FreeAndNil(Container);
end;

function TCamper.Serialize(): AnsiString;
var
  Container: TStringList;
begin
  Container := TStringList.Create();

  Container.Values['number']      := IntToStr(Self.Number);
  Container.Values['camperclass'] := IntToStr(Self.CamperClass);
  Container.Values['plate']       := Self.Plate;

  Result := Container.DelimitedText;
  
  FreeAndNil(Container);
end;

function TCamper.GetString(Column: ShortString): String;
begin
  Result := '';
  
  if Column = 'number' then
    Result := IntToStr(Self.Number);
  if Column = 'plate' then
    Result := Self.Plate;
  if Column = 'class' then
    Result := Data.CamperClasses[Self.CamperClass].Name;
end;

function TCamper.GetNumber(Column: ShortString): Single;
begin
  Result := 0;
  if Column = 'number' then
    Result := Self.Number;
  if Column = 'power' then
    Result := Data.CamperClasses[Self.CamperClass].Power;
  if Column = 'engine' then
    Result := Data.CamperClasses[Self.CamperClass].Engine;
end;

function TCamper.GetBoolean(Column: ShortString): Boolean;
begin
  Result := false;
end;

end.
