unit UReservationsCustomer;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UListModel,
  UListView,
  UPaging;

type

TReservationsCustomer = class
  public
    FCustomer:  Integer;
    FListView:  TListView;
    FPaging:    TPaging;
    
    constructor Create(Customer: Integer);
    function    GetList: TListModel;
    function    SelectedReservation: Integer;
    procedure   Show;
  
  private
    procedure DrawInterface;
end;

procedure ShowReservationsByCustomer(Customer: Integer);
function  SorterFunc(A,B: IListRow): Boolean;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UData,
  UDelete,
  UReservations,
  UReservationEdit,
  UReservationView,
  UInvoice,
  UStringUtils;

constructor TReservationsCustomer.Create(Customer: Integer);
var
  List: TListModel;
begin
  Self.FCustomer := Data.Customers[Customer].Number;
  List := Self.GetList;
  Self.FPaging := TPaging.Create(Length(List), 10, false);
  Self.FListView := TListView.Create(Self.FPaging, Columns, List, false);
end;

procedure TReservationsCustomer.Show;
var
  KeyCode, ScanCode: Char;
  Redraw: Boolean;
begin
  Redraw := true;
  
  while Redraw do
  begin
    Self.DrawInterface;
    
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        ScanCode := ReadKey;
        case ScanCode of
          #72: // Pfeil hoch
            Self.FPaging.SelectPrev;
          
          #73: // Bild hoch
            Self.FPaging.BrowsePrev;
          
          #75: // Pfeil links
            Self.FPaging.BrowsePrev;
          
          #77: // Pfeil rechts
            Self.FPaging.BrowseNext;
          
          #80: // Pfeil runter
            Self.FPaging.SelectNext;
          
          #81: // Bild runter
            Self.FPaging.BrowseNext;
        end;
      end;
      #13: // Enter
        ReservationView(Self.SelectedReservation);
      
      'd':
      begin
        DeleteReservation(Self.SelectedReservation);
        Self.FListView.FListModel := Self.GetList;
        Self.FPaging.Length := Length(Self.FListView.FListModel);
      end;
      
      'n':
      begin
        ReservationCreate(0, Data.GetCustomerByNumber(Self.FCustomer), 0, 1);
        Self.FListView.FListModel := Self.GetList;
        Self.FPaging.Length := Length(Self.FListView.FListModel);
      end;
      
      'e':
      begin
        ReservationEdit(Self.SelectedReservation);
        Self.FListView.FListModel := Self.GetList;
        Self.FPaging.Length := Length(Self.FListView.FListModel);
      end;
      
      'r':
        InvoiceView(Self.SelectedReservation);
      
      #27: // Esc
        Redraw := false;
    end;
  end;
end;

procedure TReservationsCustomer.DrawInterface;
begin
  PrintHead('Buchungen');
  PrintHelp(KeysSelect);
  PrintHelp(KeysEdit);
  PrintLn;
  
  Self.FListView.Draw;
  
  Cursor(1, Self.FListView.FSelectedRow);
end;

function TReservationsCustomer.GetList: TListModel;
var
  I, Len: Integer;
begin
  SetLength(Result, 0);
  Len := 0;
  for I := 0 to High(Data.Reservations) do
  begin
    if Data.Reservations[I].Customer = Self.FCustomer then
    begin
      inc(Len);
      SetLength(Result, Len);
      Result[Len-1] := Data.Reservations[I];
    end;
  end;
  
  InsertionSort(Result, SorterFunc);
end;

function TReservationsCustomer.SelectedReservation: Integer;
var
  Reservation: IListRow;
begin
  Reservation := Self.FListView.FListModel[Self.FPaging.SelectedIndex];
  Result := Trunc(Reservation.GetNumber('index'));
end;

procedure ShowReservationsByCustomer(Customer: Integer);
var
  ReservationsCustomer: TReservationsCustomer;
begin
  ReservationsCustomer := TReservationsCustomer.Create(Customer);
  ReservationsCustomer.Show;
end;

function SorterFunc(A,B: IListRow): Boolean;
begin
  if A.GetNumber('startweek') > B.GetNumber('startweek') then
    Result := true
  else if A.GetNumber('startweek') < B.GetNumber('startweek') then
    Result := false
  else
  begin
    if A.GetNumber('endweek') > B.GetNumber('endweek') then
      Result := true
    else if A.GetNumber('endweek') < B.GetNumber('endweek') then
      Result := false
    else
    begin
      if A.GetNumber('camper') > B.GetNumber('camper') then
        Result := true
      else
        Result := false;
    end;
  end;
end;

end.

