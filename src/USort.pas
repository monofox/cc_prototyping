unit USort;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

procedure SortCampers;
procedure SortCustomers;

implementation

uses
  UCamper,
  UCustomer,
  UData;

procedure SortCampers;
var
  I,J: Integer;
  Tmp: TCamper;
begin
  for I := 1 to High(Data.Campers) do
  begin
    Tmp := Data.Campers[I];
    J := I;
    while (J > 0) and (Data.Campers[J-1].Number > Tmp.Number) do
    begin
      Data.Campers[J] := Data.Campers[J - 1];
      dec(J);
    end;
    Data.Campers[J] := Tmp;
  end;
end;

procedure SortCustomers;
var
  I,J: Integer;
  Tmp: TCustomer;
begin
  for I := 1 to High(Data.Customers) do
  begin
    Tmp := Data.Customers[I];
    J := I;
    while (J > 0) and (Data.Customers[J-1].Number > Tmp.Number) do
    begin
      Data.Customers[J] := Data.Customers[J - 1];
      dec(J);
    end;
    Data.Customers[J] := Tmp;
  end;
end;

end.
