unit UListModel;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

type

IListRow = interface
  function GetBoolean(Column: ShortString): Boolean;
  function GetNumber (Column: ShortString): Single;
  function GetString (Column: ShortString): String;
end;

TListModel = array of IListRow;

TSimpleListRow = class(TInterfacedObject, IListRow)
  FLabel: String;
  
  constructor Create(ALabel: String);
  function GetBoolean(Column: ShortString): Boolean;
  function GetNumber (Column: ShortString): Single;
  function GetString (Column: ShortString): String;
end;

implementation

constructor TSimpleListRow.Create(ALabel: String);
begin
  Self.FLabel := ALabel;
end;

function TSimpleListRow.GetString(Column: ShortString): String;
begin
  Result := Self.FLabel;
end;

function TSimpleListRow.GetNumber(Column: ShortString): Single;
begin
  Result := 0;
end;

function TSimpleListRow.GetBoolean(Column: ShortString): Boolean;
begin
  Result := false;
end;

end.


