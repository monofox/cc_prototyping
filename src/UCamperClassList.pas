unit UCamperClassList;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UPaging,
  UListView;

type

TCamperClassList = class
  public
    FListView: TListView;
    FPaging:   TPaging;
    
    constructor Create;
    procedure Show;
  
  private
    procedure DrawInterface;
end;

procedure ShowCamperClassList;

implementation

uses
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF},
  UCamperClassEdit,
  UData,
  UDelete,
  UStringUtils;

var
  KeysSelect: array[0..1] of TKey =
  (
    ('Leertaste', 'Auswählen'),
    ('Esc', 'Zurück')
  );
  KeysEdit: array[0..2] of TKey =
  (
    ('n', 'Neuer Camper-Typ'),
    ('e', 'Bearbeiten'),
    ('d', 'Löschen')
  );
  KeysCreate: array[0..1] of TKey =
  (
    ('Esc', 'Zurück'),
    ('Enter', 'Camper-Typ anlegen')
  );
  Columns: array[0..5] of TColumn =
  (
    (Name: 'Name';       Content: SString; Source: 'name';    Length: 10; Padding: true),
    (Name: 'Grundpreis'; Content: SNumber; Source: 'price';   Length: 10; Padding: true; Decimals: 2),
    (Name: 'Leistung';   Content: SNumber; Source: 'power';   Length:  8; Padding: true; Decimals: 0),
    (Name: 'Hubraum';    Content: SNumber; Source: 'engine';  Length:  7; Padding: true; Decimals: 1),
    (Name: 'Variante';   Content: SString; Source: 'variant'; Length: 10; Padding: true),
    (Name: 'Plätze';     Content: SNumber; Source: 'size';    Length:  7; Padding: true; Decimals: 0)
  );

constructor TCamperClassList.Create;
begin
  Self.FPaging := TPaging.Create(Length(Data.CamperClasses), 10, true);
  Self.FListView := TListView.Create(Self.FPaging, Columns, Data.ListCamperClasses, true);
end;

procedure TCamperClassList.Show;
var
  KeyCode, ScanCode: Char;
  Redraw: Boolean;
begin
  Redraw := true;
  
  while Redraw do
  begin
    Self.DrawInterface;
    
    KeyCode := ReadKey;
    
    case KeyCode of
      #0:
      begin
        ScanCode := ReadKey;
        case ScanCode of
          #72: // Pfeil hoch
            Self.FPaging.SelectPrev;
          
          #73: // Bild hoch
            Self.FPaging.BrowsePrev;
          
          #75: // Pfeil links
            Self.FPaging.BrowsePrev;
          
          #77: // Pfeil rechts
            Self.FPaging.BrowseNext;
          
          #80: // Pfeil runter
            Self.FPaging.SelectNext;
          
          #81: // Bild runter
            Self.FPaging.BrowseNext;
        end;
      end;
      
      ' ':
      begin
        if Self.FPaging.IsSelected(Self.FPaging.SelectedIndex) then
          Self.FPaging.Unselect(Self.FPaging.SelectedIndex)
        else
          Self.FPaging.Select(Self.FPaging.SelectedIndex);
      end;
      
      'd':
      begin
        DeleteCamperClass(Self.FPaging);
        Self.FListView.FListModel := Data.ListCamperClasses;
        if Self.FPaging.Length = 0 then
          Redraw := false;
      end;
      
      'n':
      begin
        CamperClassCreate;
        Self.FPaging.Length := Length(Data.CamperClasses);
        Self.FListView.FListModel := Data.ListCamperClasses;
      end;
      
      'e':
      begin
        CamperClassEdit(Self.FPaging);
        Self.FPaging.Length := Length(Data.CamperClasses);
        Self.FListView.FListModel := Data.ListCamperClasses;
      end;
      
      #27: // Esc
        Redraw := false;
    end;
  end;
end;

procedure TCamperClassList.DrawInterface;
begin
  PrintHead('Übersicht Camper-Typen');
  PrintHelp(KeysSelect);
  PrintHelp(KeysEdit);
  PrintLn;
  
  Self.FListView.Draw;
  
  Cursor(1, Self.FListView.FSelectedRow);
end;

procedure ShowCamperClassList;
var
  CamperClassList: TCamperClassList;
begin
  if Length(Data.CamperClasses) = 0 then
  begin
    PrintHead('Übersicht Camper-Typen');
    PrintHelp(KeysCreate);
    PrintLn;
    PrintLn('Es sind noch keine Camper-Typen festgelegt.');
    while true do
    begin
      case ReadKey of
        #0:
          ReadKey;
        
        #13:
        begin
          CamperClassCreate;
          break;
        end;
        
        #27:
          break;
      end;
    end;
  end;
  
  if Length(Data.CamperClasses) > 0 then
  begin
    CamperClassList := TCamperClassList.Create;
    CamperClassList.Show;
  end;
end;

end.

