unit UPaging;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  SysUtils;

type

TNoEntriesException = class(Exception);
TNoMultiSelectionException = class(Exception);

TPaging = class
  private
    FLength:            Integer;
    FPage:              Integer;
    FEntriesPerPage:    Integer;
    FMaxPage:           Integer;
    FStartIndex:        Integer;
    FEndIndex:          Integer;
    FSelectedIndex:     Integer;
    FHasMultiSelection: Boolean;
    FSelection:         array of Boolean;
    
    procedure Update;
    procedure WriteLength(ALength: Integer);
    procedure WriteSelectedIndex(AIndex: Integer);
  
  public
    constructor Create(ALength, AEntriesPerPage: Integer; AHasMultiSelection: Boolean);
    procedure BrowseNext;
    procedure BrowsePrev;
    function  CountSelected: Integer;
    function  IsSelected(Index: Integer): Boolean;
    procedure Select(Index: Integer);
    procedure SelectNext;
    procedure SelectPrev;
    procedure Unselect(Index: Integer);
    
    property Length:        Integer read FLength write WriteLength;
    property Page:          Integer read FPage;
    property MaxPage:       Integer read FMaxPage;
    property StartIndex:    Integer read FStartIndex;
    property EndIndex:      Integer read FEndIndex;
    property SelectedIndex: Integer read FSelectedIndex write WriteSelectedIndex;
end;

implementation

uses
  Math;

constructor TPaging.Create(ALength, AEntriesPerPage: Integer; AHasMultiSelection: Boolean);
var
  I: Integer;
begin
  if ALength = 0 then
    raise TNoEntriesException.Create('TPaging cannot handle empty lists!');
  
  Self.FLength            := ALength;
  Self.FHasMultiSelection := AHasMultiSelection;
  Self.FEntriesPerPage    := AEntriesPerPage;
  Self.FPage              := 0;
  Self.FSelectedIndex     := 0;
  
  SetLength(Self.FSelection, Self.FLength);
  if (Self.FLength > 0) and Self.FHasMultiSelection then
    for I := 0 to High(Self.FSelection) do
      Self.FSelection[I] := false;
  
  Update;
end;

procedure TPaging.BrowseNext;
begin
  if Self.FPage < Self.FMaxPage then
  begin
    Inc(Self.FPage);
    Update;
    
    if Self.FSelectedIndex < Self.FStartIndex then
      Self.FSelectedIndex := Self.FStartIndex;
  end;
end;

procedure TPaging.BrowsePrev;
begin
  if Self.FPage > 0 then
  begin
    Dec(Self.FPage);
    Update;
    
    if Self.FSelectedIndex > Self.FEndIndex then
      Self.FSelectedIndex := Self.FEndIndex;
  end;
end;

function TPaging.CountSelected: Integer;
var
  I: Integer;
begin
  Result := 0;
  
  for I := 0 to High(Self.FSelection) do
    if Self.FSelection[I] then
      Inc(Result);
end;

function TPaging.IsSelected(Index: Integer): Boolean;
begin
  if not Self.FHasMultiSelection then
    raise TNoMultiSelectionException.Create('No Multi-Selection');
  
  Result := Self.FSelection[Index];
end;

procedure TPaging.SelectNext;
begin
  if Self.FSelectedIndex < Self.FLength - 1 then
  begin
    Inc(Self.FSelectedIndex);
    
    if Self.FSelectedIndex > Self.FEndIndex then
      BrowseNext;
    
    Update;
  end;
end;

procedure TPaging.SelectPrev;
begin
  if Self.FSelectedIndex > 0 then
  begin
    Dec(Self.FSelectedIndex);
    
    if Self.FSelectedIndex < Self.FStartIndex then
      BrowsePrev;
    
    Update;
  end;
end;


procedure TPaging.Update;
begin
  Self.FMaxPage := ceil(Self.FLength / Self.FEntriesPerPage) - 1;
  if Self.FPage > Self.FMaxPage then
    Self.FPage := Self.FMaxPage;
  
  Self.FStartIndex := Self.FEntriesPerPage * Self.FPage;
  Self.FEndIndex := Self.FEntriesPerPage * (Self.FPage + 1) - 1;
  
  if Self.FEndIndex >= Self.FLength then
    Self.FEndIndex := Self.FLength - 1;
end;

procedure TPaging.WriteLength(ALength: Integer);
var
  I: Integer;
begin
  if ALength = 0 then
    raise TNoEntriesException.Create('TPaging cannot handle empty lists!');
  
  if (ALength > Self.FLength) and Self.FHasMultiSelection then
  begin
    SetLength(Self.FSelection, ALength);
    for I := Self.FLength to High(Self.FSelection) do
      Self.FSelection[I] := false;
  end;
  
  Self.FLength := ALength;
  Update;
  
  if Self.FSelectedIndex < Self.FStartIndex then
    Self.FSelectedIndex := Self.FStartIndex;
  
  if Self.FSelectedIndex > Self.FEndIndex then
    Self.FSelectedIndex := Self.FEndIndex;
end;

procedure TPaging.WriteSelectedIndex(AIndex: Integer);
begin
  if (AIndex >= 0) and (AIndex < Self.FLength) then
  begin
    Self.FSelectedIndex := AIndex;
    
    // Nicht Self.FSelectedIndex verwenden, wird von den Browse-Prozeduren geändert
    while AIndex < Self.FStartIndex do
      BrowsePrev;
    while AIndex > Self.FEndIndex do
      BrowseNext;
    
    Update;
  end;
end;

procedure TPaging.Unselect(Index: Integer);
begin
  if not Self.FHasMultiSelection then
    raise TNoMultiSelectionException.Create('No Multi-Selection');
  
  Self.FSelection[Index] := false;
end;

procedure TPaging.Select(Index: Integer);
begin
  if not Self.FHasMultiSelection then
    raise TNoMultiSelectionException.Create('No Multi-Selection');
  
  Self.FSelection[Index] := true;
end;

end.

