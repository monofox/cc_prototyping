unit UCustomer;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UListModel;

type

TCustomer = class(TInterfacedObject, IListRow)
  public
    Number: Integer;
    Name, FirstName, Phone: String;
    
    constructor Create(ANumber: Integer; AName, AFirstName, APhone: String);
    constructor Deserialize(Data: AnsiString);
    function    GetBoolean(Column: ShortString): Boolean;
    function    GetNumber (Column: ShortString): Single;
    function    GetString (Column: ShortString): String;
    function    Serialize: AnsiString;
end;

TCustomers = array of TCustomer;

implementation

uses
  Classes,
  SysUtils,
  UData;

constructor TCustomer.Create(ANumber: Integer; AName, AFirstName, APhone: String);
begin
  Self.Number    := ANumber;
  Self.Name      := AName;
  Self.FirstName := AFirstName;
  Self.Phone     := APhone;
end;

constructor TCustomer.Deserialize(Data: AnsiString);
var
  Container: TStringList;
begin
  Container := TStringList.Create;
  Container.DelimitedText := Data;

  Self.Number    := StrToInt(Container.Values['number']);
  Self.Name      := Container.Values['name'];
  Self.FirstName := Container.Values['firstname'];
  Self.Phone     := Container.Values['phone'];

  FreeAndNil(Container);
end;

function TCustomer.Serialize(): AnsiString;
var
  Container: TStringList;
begin
  Container := TStringList.Create();

  Container.Values['number']    := IntToStr(Self.Number);
  Container.Values['name']      := Self.Name;
  Container.Values['firstname'] := Self.FirstName;
  Container.Values['phone']     := Self.Phone;

  Result := Container.DelimitedText;
  
  FreeAndNil(Container);
end;

function TCustomer.GetString(Column: ShortString): String;
begin
  Result := '';
  
  if Column = 'name' then
    Result := Self.Name;
  if Column = 'firstname' then
    Result := Self.FirstName;
  if Column = 'phone' then
    Result := Self.Phone;
  if Column = 'group' then
    Result := Data.GetCustomerTypeByCustomer(Self).Name;
  if Column = 'fullname' then
    Result := Format('%s, %s', [Self.Name, Self.FirstName]);
end;

function TCustomer.GetNumber(Column: ShortString): Single;
begin
  Result := 0;
  if Column = 'number' then
    Result := Self.Number;
end;

function TCustomer.GetBoolean(Column: ShortString): Boolean;
begin
  Result := false;
end;

end.
