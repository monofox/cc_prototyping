unit UCustomerType;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

type

TCustomerType = class
  public
    MinNum: Integer;
    Discount: Single;
    Name: String;
    constructor Deserialize(Data: AnsiString);
end;

TCustomerTypes = array of TCustomerType;

implementation

uses
  Classes,
  SysUtils;

constructor TCustomerType.Deserialize(Data: AnsiString);
var
  Container: TStringList;
begin
  Container := TStringList.Create;
  Container.DelimitedText := Data;
  
  Self.MinNum   := StrToInt(Container.Values['minnum']);
  Self.Discount := StrToFloat(Container.Values['discount']);
  Self.Name     := Container.Values['name'];
  
  FreeAndNil(Container);
end;

end.
