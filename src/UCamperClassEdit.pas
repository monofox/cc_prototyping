unit UCamperClassEdit;
// vi:fenc=utf-8:tabstop=2:shiftwidth=2:smartindent:smarttab:expandtab

interface

uses
  UCamperClass,
  UInput,
  UInputButton,
  UInputNumber,
  UInputSelect,
  UInputText,
  UPaging;

type

TCamperClassEditor = class
  private
    FCamperClass: TCamperClass;
    
    FSize:        TInputNumber;
    FVariant:     TInputSelect;
    FName:        TInputText;
    FPrice:       TInputNumber;
    FEngine:      TInputNumber;
    FPower:       TInputNumber;
    
    FBtnCancel:   TInputButton;
    FBtnCont:     TInputButton;
    FBtnSave:     TInputButton;
    FMultiple:    Boolean;
  
  public
    constructor Create(CamperClass: TCamperClass; Multiple: Boolean);
    procedure   Draw;
    function    Save: Boolean;
    function    Show: TInputAction;
end;

procedure CamperClassCreate;
procedure CamperClassEdit(Paging: TPaging);

implementation

uses
  UData,
  UListModel,
  UListView,
  UStringUtils,
  {$IFDEF FPC}Crt{$ELSE}Console{$ENDIF};

var
  Keys: array[0..1] of TKey = 
  (
    ('Tab', 'Zum nächsten Feld'),
    ('Esc', 'Zurück')
  );
  Columns: array[0..0] of TColumn = 
  (
    (Name: 'Name';       Content: SString; Source: 'name';    Length: 10; Padding: true)
  );
  ErrorMsg: array of String;

constructor TCamperClassEditor.Create(CamperClass: TCamperClass; Multiple: Boolean);
var
  ListModel: TListModel;
begin
  Self.FCamperClass := CamperClass;
  
  Self.FMultiple := Multiple;
  
  Self.FBtnCancel := TInputButton.Create('Abbrechen');
  Self.FBtnSave   := TInputButton.Create('Speichern');
  
  ListModel := Data.ListCamperClasses;
  
  if CamperClass = nil then
  begin
    Self.FBtnCont := TInputButton.Create('Speichern und weiteren Camper-Typ erstellen');
    
    Self.FSize    := TInputNumber.Create(2, 1, 30, true);
    Self.FVariant := TInputSelect.Create('Ausführung', Columns, ListModel, 0);
    Self.FName    := TInputText.Create('', 100, 10);
    Self.FPrice   := TInputNumber.Create(0, 0, 100000, false);
    Self.FEngine  := TInputNumber.Create(0, 0, 1000, false);
    Self.FPower   := TInputNumber.Create(0, 0, 1000, false);
  end
  else
  begin
    Self.FBtnCont := TInputButton.Create('Speichern und nächsten Camper-Typ bearbeiten');
    
    Self.FSize    := TInputNumber.Create(CamperClass.Size,    1, 30, true);
    Self.FVariant := TInputSelect.Create('Ausführung', Columns, ListModel, Integer(CamperClass.Variant));
    Self.FName    := TInputText.Create(CamperClass.Name,    100, 10);
    Self.FPrice   := TInputNumber.Create(CamperClass.Price,   0, 100000, false);
    Self.FEngine  := TInputNumber.Create(CamperClass.Engine,  0, 1000, false);
    Self.FPower   := TInputNumber.Create(CamperClass.Power,   0, 1000, false);
  end;
  
  Self.FEngine.StepSmall := 0.1;
  Self.FEngine.StepLarge := 1;
end;

procedure TCamperClassEditor.Draw;
var
  I: Integer;
begin
  PrintHead('Camper-Typ bearbeiten');
  PrintHelp(Keys);
  PrintLn;
  
  Print('Bezeichnung:  ');
  Self.FName.SetPosition;
  Self.FName.Draw;
  PrintLn;
  
  Print('Schlafplätze:');
  Self.FSize.SetPosition;
  Self.FSize.Draw;
  PrintLn;
  
  Print('Ausführung:  ');
  Self.FVariant.SetPosition;
  Self.FVariant.Draw;
  PrintLn;
  
  Print('Grundpreis:   ');
  Self.FPrice.SetPosition;
  Self.FPrice.Draw;
  PrintLn;
  
  Print('Verbrauch:    ');
  Self.FEngine.SetPosition;
  Self.FEngine.Draw;
  PrintLn;
  
  Print('Leistung:     ');
  Self.FPower.SetPosition;
  Self.FPower.Draw;
  PrintLn;
  
  if Self.FMultiple then
  begin
    PrintLn;
    Self.FBtnCont.SetPosition;
    Self.FBtnCont.Draw;
  end;
  
  PrintLn;
  Self.FBtnSave.SetPosition;
  Print('               ');
  Self.FBtnCancel.SetPosition;
  
  Self.FBtnSave.Draw;
  Self.FBtnCancel.Draw;

  // Draw error messages
  if (Length(ErrorMsg) > 0) then
  begin
    PrintLn;
    PrintLn;
    TextColor(Red);
    PrintLn('Beim Speichern sind folgende Fehler aufgetreten:');
    for I := 0 to High(ErrorMsg) do
    begin
      PrintLn('> ' + ErrorMsg[I]);
    end;
    NormVideo;
  end;
end;

function TCamperClassEditor.Show: TInputAction;
begin
  Result := SDiscard;
  // At the beginning, we have no errors.
  SetLength(ErrorMsg, 0);

  while true do
  begin
    Self.Draw;
    
    if Self.FName.Activate = SEscape then
      break;
    
    if Self.FSize.Activate = SEscape then
      break;
    
    if Self.FVariant.Activate = SEscape then
      break;
    Self.Draw; // TInputSelect überschreibt möglicherweise den Bildschirm
    
    if Self.FPrice.Activate = SEscape then
      break;
    
    if Self.FEngine.Activate = SEscape then
      break;
    
    if Self.FPower.Activate = SEscape then
      break;
    
    if Self.FMultiple then
    begin
      case Self.FBtnCont.Activate of
        SSpace, SReturn:
        begin
          if Self.Save then
          begin
            Result := SCont;
            break
          end;
          Self.Draw;
        end;
        
        SEscape:
          break;
      end;
    end;
    
    case Self.FBtnSave.Activate of
      SSpace, SReturn:
      begin
        if Self.Save then
        begin
          Result := SSave;
          break;
        end;
        Self.Draw;
      end;
      
      SEscape:
        break;
    end;
    
    if Self.FBtnCancel.Activate <> STab then
      break;
  end;
end;

function TCamperClassEditor.Save: Boolean;
var
  CamperClass: TCamperClass;
begin
  Result := true;
  SetLength(ErrorMsg, 0);

  // Set message
  if Self.FEngine.Value <= 0 then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Der Wert für "Verbrauch" muss größer 0 sein!';
    Result := false;
  end;

  if Trunc(Self.FPower.Value) <= 0 then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Der Wert für "Leistung" muss > 0 sein!';
    Result := false;
  end;

  if Length(Self.FName.Value) <= 0 then
  begin
    SetLength(ErrorMsg, Length(ErrorMsg) + 1);
    ErrorMsg[High(ErrorMsg)] := 'Die Eingabe der "Bezeichnung" ist Pflicht!';
    Result := false;
  end
  else
  begin
    // Check whether the name does already exist!
    // Only check, if its not changed
    if ((Self.FCamperClass <> nil) and (Self.FCamperClass.Name <> Self.FName.Value)) or
       (Self.FCamperClass = nil) then
    begin
      if Data.GetCamperClassByName(Self.FName.Value) > -1 then
      begin
        SetLength(ErrorMsg, Length(ErrorMsg) + 1);
        ErrorMsg[High(ErrorMsg)] := 'Es existiert bereits eine Camper-Typ mit der gegebenen Bezeichnung!';
        Result := false; 
      end;
    end;
  end;

  if ((Self.FCamperClass = nil) and Result) then
  begin
    CamperClass := TCamperClass.Create(Self.FName.Value,
                                       Trunc(Self.FSize.Value),
                                       TCamperVariant(Self.FVariant.Value),
                                       Self.FPrice.Value,
                                       Self.FEngine.Value,
                                       Trunc(Self.FPower.Value));
    Data.AddCamperClass(CamperClass);
  end
  else if (Result) then
  begin
    Self.FCamperClass.Name    := Self.FName.Value;
    Self.FCamperClass.Size    := Trunc(Self.FSize.Value);
    Self.FCamperClass.Variant := TCamperVariant(Self.FVariant.Value);
    Self.FCamperClass.Price   := Self.FPrice.Value;
    Self.FCamperClass.Engine  := Self.FEngine.Value;
    Self.FCamperClass.Power   := Trunc(Self.FPower.Value);
  end;
end;

procedure CamperClassCreate;
var
  Editor: TCamperClassEditor;
begin
  while true do
  begin
    Editor := TCamperClassEditor.Create(nil, true);
    if Editor.Show <> SCont then
      break;
  end;
end;

procedure CamperClassEdit(Paging: TPaging);
var
  Editor: TCamperClassEditor;
  Count, I: Integer;
begin
  Count := Paging.CountSelected;
  if Count = 0 then
  begin
    Editor := TCamperClassEditor.Create(Data.CamperClasses[Paging.SelectedIndex], false);
    Editor.Show;
  end
  else
  begin
    for I := 0 to Paging.Length - 1 do
    begin
      if not Paging.IsSelected(I) then
        continue;
      
      Editor := TCamperClassEditor.Create(Data.CamperClasses[I], true);
      Editor.Show;
    end;
  end;
end;

end.

